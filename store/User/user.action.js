const userAction = {
    USER_PAGE_LOADING: '[User] User Page Loading',
    USER_ACTION_LOADING: '[User] User Action Loading',
    FIND_USERS_REQUESTED: '[User] Find Users Requested',
    FIND_USERS_SUCCESS: '[User] Find Users Success',
    CREATE_NEW_USER_REQUESTED: '[User] Create New User Requested',
    CREATE_NEW_USER_SUCCESS: '[User] Create New User Success',
    OPEN_USER_MODAL: '[User] Open User Modal',
    HIDE_USER_MODAL: '[User] Hide User Modal',
    USER_CHECK_EMAIL_REQUESTED: '[User] Check Email Requested',
    USER_CHECK_EMAIL_SUCCESS: '[User] Check Email Success',
    USER_VALIDATING_EMAIL: '[User] Validating Email',
    USER_CHECK_USERNAME_REQUESTED: '[User] Check Username Requested',
    USER_CHECK_USERNAME_SUCCESS: '[User] Check Username Success',
    USER_VALIDATING_USERNAME: '[User] Validating Username',
    UPDATE_USER_REQUESTED: '[User] Update User Requested',
    UPDATE_USER_SUCCESS: '[User] Update User Success',

    userPageLoading: (isLoading) => {
        return {
            type: userAction.USER_PAGE_LOADING,
            payload: {isLoading}
        }
    },

    userActionLoading: (isLoading) => {
        return {
            type: userAction.USER_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    findUsersRequested: (query) => {
        return {
            type: userAction.FIND_USERS_REQUESTED,
            payload: {query}
        }
    },

    findUsersSuccess: (users, lastQuery, lastQueryResult) => {
        return {
            type: userAction.FIND_USERS_SUCCESS,
            payload: {users, lastQueryResult, lastQuery}
        }
    },

    createNewUserRequested: (user) => {
        return {
            type: userAction.CREATE_NEW_USER_REQUESTED,
            payload: {user}
        }
    },

    createNewUserSuccess: (user) => {
        return {
            type: userAction.CREATE_NEW_USER_SUCCESS,
            payload: {user}
        }
    },

    openUserModal: () => {
        return {
            type: userAction.OPEN_USER_MODAL
        }
    },

    hideUserModal: () => {
        return {
            type: userAction.HIDE_USER_MODAL
        }
    },

    checkEmailRequested: (email) => {
        return {
            type: userAction.USER_CHECK_EMAIL_REQUESTED,
            payload: {email}
        }
    },

    checkEmailSuccess: (valid) => {
        return {
            type: userAction.USER_CHECK_EMAIL_SUCCESS,
            payload: {valid}
        }
    },

    validatingEmail: (isValidating) => {
        return {
            type: userAction.USER_VALIDATING_EMAIL,
            payload: {isValidating}
        }
    },

    checkUsernameRequested: (username) => {
        return {
            type: userAction.USER_CHECK_USERNAME_REQUESTED,
            payload: {username}
        }
    },

    checkUsernameSuccess: (valid) => {
        return {
            type: userAction.USER_CHECK_USERNAME_SUCCESS,
            payload: {valid}
        }
    },

    validatingUsername: (isValidating) => {
        return {
            type: userAction.USER_VALIDATING_USERNAME,
            payload: {isValidating}
        }
    },

    updateUserRequested: (user, id) => {
        return {
            type: userAction.UPDATE_USER_REQUESTED,
            payload: {user, id}
        }
    },

    updateUserSuccess: (user) => {
        return {
            type: userAction.UPDATE_USER_SUCCESS,
            payload: {user}
        }
    }
};

export default userAction;
