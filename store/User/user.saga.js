import {all, call, put, takeEvery} from 'redux-saga/effects'
import userAction from "./user.action";
import {checkEmail, checkUsername, createNewUser, findAllUsers, updateUser} from "../../helper/user.helper";
import {notification} from "antd";

function* findAllUsersWatch({payload}) {
    try {
        yield put(userAction.userPageLoading(true));
        const response = yield call(findAllUsers, payload.query);
        yield put(userAction.findUsersSuccess(response.data, payload.query, response.page));
        yield put(userAction.userPageLoading(false));
    } catch (e) {
        yield put(userAction.userPageLoading(false));
    }
}

function* createNewUserWatch({payload}) {
    try {
        yield put(userAction.userActionLoading(true));
        const user = yield call(createNewUser, payload.user);
        yield put(userAction.createNewUserSuccess(user));
        notification['success']({
            message: 'New User Created!',
            description: `New user "@${user.username}" created!`
        });
        yield put(userAction.hideUserModal());
        yield put(userAction.userActionLoading(false));
    } catch (e) {
        notification['error']({
            message: 'Failed Creating User',
            description: `Failed creating new user, please try again`,
        });
        yield put(userAction.userActionLoading(false));
    }
}

function* checkEmailWatch({payload}) {
    try {
        yield put(userAction.validatingEmail(true));
        const valid = yield call(checkEmail, payload.email);
        yield put(userAction.checkEmailSuccess(valid.status));
        yield put(userAction.validatingEmail(false));
    } catch (e) {
        yield put(userAction.validatingEmail(false));
    }
}

function* checkUsernameWatch({payload}) {
    try {
        yield put(userAction.validatingUsername(true));
        const valid = yield call(checkUsername, payload.username);
        yield put(userAction.checkUsernameSuccess(valid.status));
        yield put(userAction.validatingUsername(false));
    } catch (e) {
        yield put(userAction.validatingUsername(false));
    }
}

function* updateUserWatch({payload}) {
    try {
        yield put(userAction.userActionLoading(true));
        const user = yield call(updateUser, payload.user, payload.id);
        yield put(userAction.updateUserSuccess(user));
        notification['success']({
            message: 'User Updated!',
            description: `User "@${user.username}" updated!`
        });
        yield put(userAction.hideUserModal());
        yield put(userAction.userActionLoading(false));
    } catch (e) {
        notification['error']({
            message: 'Failed Updating User',
            description: `Failed updating user, please try again`,
        });
        yield put(userAction.userActionLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(userAction.FIND_USERS_REQUESTED, findAllUsersWatch),
        takeEvery(userAction.CREATE_NEW_USER_REQUESTED, createNewUserWatch),
        takeEvery(userAction.USER_CHECK_EMAIL_REQUESTED, checkEmailWatch),
        takeEvery(userAction.USER_CHECK_USERNAME_REQUESTED, checkUsernameWatch),
        takeEvery(userAction.UPDATE_USER_REQUESTED, updateUserWatch),
    ]);
}
