import userAction from "./user.action";
import produce from "immer";

const initialState = {
    actionLoading: false,
    pageLoading: false,
    users: [],
    dataLoaded: false,
    lastCreatedUser: null,
    lastQuery: {},
    lastQueryResult: {},
    isModalOpen: false,
    validateEmail: {status: false, loading: false},
    validateUsername: {status: false, loading: false}
}

export default function userReducer(state = initialState, {type, payload}) {
    switch (type) {
        case userAction.USER_PAGE_LOADING:
            return {
                ...state,
                pageLoading: payload.isLoading
            }
        case userAction.USER_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            }
        case userAction.FIND_USERS_SUCCESS:
            return {
                ...state,
                users: payload.users,
                lastQuery: payload.lastQuery,
                lastQueryResult: payload.lastQueryResult,
                dataLoaded: true
            }
        case userAction.CREATE_NEW_USER_SUCCESS:
            return {
                ...state,
                lastCreatedUser: payload.user,
                users: produce(state.users, draftState => {
                    draftState.unshift(payload.user);
                })
            }
        case userAction.OPEN_USER_MODAL:
            return {
                ...state,
                isModalOpen: true
            }
        case userAction.HIDE_USER_MODAL:
            return {
                ...state,
                isModalOpen: false
            }
        case userAction.USER_VALIDATING_EMAIL:
            return {
                ...state,
                validateEmail: {...state.validateEmail, loading: payload.isValidating}
            }
        case userAction.USER_CHECK_EMAIL_SUCCESS:
            return {
                ...state,
                validateEmail: {...state.validateEmail, status: payload.valid}
            }
        case userAction.USER_VALIDATING_USERNAME:
            return {
                ...state,
                validateUsername: {...state.validateUsername, loading: payload.isValidating}
            }
        case userAction.USER_CHECK_USERNAME_SUCCESS:
            return {
                ...state,
                validateUsername: {...state.validateUsername, status: payload.valid}
            }
        case userAction.UPDATE_USER_SUCCESS:
            return {
                ...state,
                users: produce(state.users, draftState => {
                    const userIndex = draftState.findIndex(user => user.id === payload.user.id);
                    if (userIndex > -1) {
                        draftState[userIndex] = payload.user;
                    }
                })
            }
        default:
            return {
                ...state
            }
    }
}
