import categoryAction from "./category.action";
import produce from "immer";

const initialState = {
    actionLoading: false,
    pageLoading: false,
    categories: [],
    lastCreatedCategory: null,
    lastQuery: {},
    lastQueryResult: {},
    isModalOpen: false,
    dataLoaded: false
}

export default function categoryReducer(state = initialState, {type, payload}) {
    switch (type) {
        case categoryAction.CATEGORY_PAGE_LOADING:
            return {
                ...state,
                pageLoading: payload.isLoading
            }
        case categoryAction.CATEGORY_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            }
        case categoryAction.CREATE_NEW_CATEGORY_SUCCESS:
            return {
                ...state,
                lastCreatedCategory: payload.category,
                categories: produce(state.categories, draftState => {
                    draftState.unshift(payload.category);
                })
            }
        case categoryAction.LOAD_ALL_CATEGORY_SUCCESS:
            return {
                ...state,
                categories: payload.categories,
                lastQuery: payload.lastQuery,
                lastQueryResult: payload.lastQueryResult,
                dataLoaded: true
            }
        case categoryAction.UPDATE_CATEGORY_SUCCESS:
            return {
                ...state,
                categories: produce(state.categories, draftState => {
                    const categoryIndex = draftState.findIndex(cat => cat.id === payload.category.id);
                    if (categoryIndex > -1) {
                        draftState[categoryIndex] = payload.category;
                    }
                })
            }
        case categoryAction.REMOVE_CATEGORY_SUCCESS:
            return {
                ...state,
                categories: produce(state.categories, draftState => {
                    const categoryIndex = draftState.findIndex(cat => cat.id === payload.category.id);
                    if (categoryIndex > -1) {
                        draftState.splice(categoryIndex, 1);
                    }
                }),
            }
        case categoryAction.OPEN_MODAL_CREATE_CATEGORY:
            return {
                ...state,
                isModalOpen: true
            }
        case categoryAction.HIDE_MODAL_CREATE_CATEGORY:
            return {
                ...state,
                isModalOpen: false
            }
        default:
            return {
                ...state
            }
    }
}
