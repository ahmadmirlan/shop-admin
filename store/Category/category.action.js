const categoryAction = {
    CATEGORY_ACTION_LOADING: '[Category] Category Action Loading',
    CATEGORY_PAGE_LOADING: '[Category] Category Page Action',
    CREATE_NEW_CATEGORY_REQUESTED: '[Category] Create New Category Requested',
    CREATE_NEW_CATEGORY_SUCCESS: '[Category] Create New Category Success',
    LOAD_ALL_CATEGORY_REQUESTED: '[Category] Load All Category Requested',
    LOAD_ALL_CATEGORY_SUCCESS: '[Category] Load All Category Success',
    REMOVE_CATEGORY_REQUESTED: '[Category] Remove Category Requested',
    REMOVE_CATEGORY_SUCCESS: '[Category] Remove Category Success',
    UPDATE_CATEGORY_REQUESTED: '[Category] Update category requested',
    UPDATE_CATEGORY_SUCCESS: '[Category] Update category success',
    OPEN_MODAL_CREATE_CATEGORY: '[Category] Open Modal Create Category',
    HIDE_MODAL_CREATE_CATEGORY: '[Category] Hide Modal Create Category',

    categoryActionLoading: (isLoading) => {
        return {
            type: categoryAction.CATEGORY_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    categoryPageLoading: (isLoading) => {
        return {
            type: categoryAction.CATEGORY_PAGE_LOADING,
            payload: {isLoading}
        }
    },

    createCategoryRequested: (category) => {
        return {
            type: categoryAction.CREATE_NEW_CATEGORY_REQUESTED,
            payload: {category}
        }
    },

    createCategorySuccess: (category) => {
        return {
            type: categoryAction.CREATE_NEW_CATEGORY_SUCCESS,
            payload: {category}
        }
    },

    loadAllCategoryRequested: (query) => {
        return {
            type: categoryAction.LOAD_ALL_CATEGORY_REQUESTED,
            payload: {query}
        }
    },

    loadAllCategorySuccess: (categories, lastQuery, lastQueryResult) => {
        return {
            type: categoryAction.LOAD_ALL_CATEGORY_SUCCESS,
            payload: {categories, lastQuery, lastQueryResult}
        }
    },

    removeCategoryRequested: (category) => {
        return {
            type: categoryAction.REMOVE_CATEGORY_REQUESTED,
            payload: {category}
        }
    },

    removeCategorySuccess: (category) => {
        return {
            type: categoryAction.REMOVE_CATEGORY_SUCCESS,
            payload: {category}
        }
    },

    updateCategoryRequested: (category) => {
        return {
            type: categoryAction.UPDATE_CATEGORY_REQUESTED,
            payload: {category}
        }
    },

    updateCategorySuccess: (category) => {
        return {
            type: categoryAction.UPDATE_CATEGORY_SUCCESS,
            payload: {category}
        }
    },

    openModalCreateCategory: () => {
        return {
            type: categoryAction.OPEN_MODAL_CREATE_CATEGORY
        }
    },

    hideModalCreateCategory: () => {
        return {
            type: categoryAction.HIDE_MODAL_CREATE_CATEGORY
        }
    },
}

export default categoryAction;
