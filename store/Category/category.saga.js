import {all, call, put, takeEvery} from 'redux-saga/effects'
import {
    createNewCategory,
    findAllCategory,
    removeCategory,
    updateCategory
} from '../../helper/category.helper';
import categoryAction from "./category.action";
import {notification} from "antd";

function* createNewCategoryWatch({payload}) {
    try {
        yield put(categoryAction.categoryActionLoading(true));
        const response = yield call(createNewCategory, payload.category);
        yield put(categoryAction.createCategorySuccess(response));
        yield put(categoryAction.categoryActionLoading(false));
        notification['success']({
            message: 'New Category Created!',
            description: `New category "${payload.category.category}" successfully created!`
        });
        yield put(categoryAction.hideModalCreateCategory());
    } catch (e) {
        notification['error']({
            message: 'Failed Creating Category!',
            description: 'Failed creating new category, please try again!'
        });
        yield put(categoryAction.categoryActionLoading(false));
    }
}

function* loadCategoryWatch({payload}) {
    try {
        yield put(categoryAction.categoryPageLoading(true));
        const response = yield call(findAllCategory);
        yield put(categoryAction.loadAllCategorySuccess(response.data, payload.query, response.page));
        yield put(categoryAction.categoryPageLoading(false));
    } catch (e) {
        yield put(categoryAction.categoryPageLoading(false));
    }
}

function* removeCategoryWatch({payload}) {
    try {
        yield put(categoryAction.categoryActionLoading(true));
        yield call(removeCategory, payload.category.id);
        yield put(categoryAction.removeCategorySuccess(payload.category));
        notification['success']({
            message: 'Category Deleted!',
            description: `Category "${payload.category.category}" successfully deleted!`
        });
        yield put(categoryAction.categoryActionLoading(false));
    } catch (e) {
        notification['error']({
            message: 'Failed Deleting Category!',
            description: 'Failed deleting category, please try again!'
        });
        yield put(categoryAction.categoryActionLoading(false));
    }
}

function* updateCategoryWatch({payload}) {
    try {
        yield put(categoryAction.categoryActionLoading(true));
        const response = yield call(updateCategory, payload.category);
        yield put(categoryAction.updateCategorySuccess(response));
        notification['success']({
            message: 'Category Updated!',
            description: `New category "${payload.category.category}" successfully updated!`
        });
        yield put(categoryAction.categoryActionLoading(false));
        yield put(categoryAction.hideModalCreateCategory());
    } catch (e) {
        notification['error']({
            message: 'Failed Updating Category!',
            description: 'Failed updating category, please try again!'
        });
        yield put(categoryAction.categoryActionLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(categoryAction.CREATE_NEW_CATEGORY_REQUESTED, createNewCategoryWatch),
        takeEvery(categoryAction.LOAD_ALL_CATEGORY_REQUESTED, loadCategoryWatch),
        takeEvery(categoryAction.REMOVE_CATEGORY_REQUESTED, removeCategoryWatch),
        takeEvery(categoryAction.UPDATE_CATEGORY_REQUESTED, updateCategoryWatch),
    ])
}
