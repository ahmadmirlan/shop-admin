import {all, call, put, takeEvery} from 'redux-saga/effects'
import permissionAction from "./permission.action";
import {
    createNewPermission,
    deletePermission,
    findAllParentPermission,
    findPermissions, getAllPermissions, updatePermission
} from "../../helper/permission.helper";
import {notification} from "antd";

function* findCategoriesWatch({payload}) {
    try {
        yield put(permissionAction.permissionPageLoading(true));
        const response = yield call(findPermissions, payload.query);
        yield put(permissionAction.findPermissionSuccess(response.data, payload.query, response.page));
        yield put(permissionAction.permissionPageLoading(false));
    } catch (e) {
        yield put(permissionAction.permissionPageLoading(false));
    }
}

function* findAllParentPermissionWatch() {
    try {
        yield put(permissionAction.permissionPageLoading(true));
        const response = yield call(findAllParentPermission);
        yield put(permissionAction.findAllParentPermissionSuccess(response));
        yield put(permissionAction.permissionPageLoading(false));
    } catch (e) {
        yield put(permissionAction.permissionPageLoading(false));
    }
}

function* createNewPermissionWatch({payload}) {
    try {
        yield put(permissionAction.permissionActionLoading(true));
        const response = yield call(createNewPermission, payload.permission);
        yield put(permissionAction.createNewPermissionSuccess(response));
        notification['success']({
            message: 'New Permission Created!',
            description: `New permission ${payload.permission.title} added!`
        });
        yield put(permissionAction.permissionActionLoading(false));
        yield put(permissionAction.hideModalPermission());
    } catch (e) {
        notification['error']({
            message: 'Failed Creating Permission!',
            description: 'Failed creating new permission, please try again!'
        });
        yield put(permissionAction.permissionActionLoading(false));
    }
}

function* deletePermissionWatch({payload}) {
    try {
        yield put(permissionAction.permissionActionLoading(true));
        yield call(deletePermission, payload.permission.id);
        yield put(permissionAction.deletePermissionSuccess(payload.permission));
        yield put(permissionAction.permissionActionLoading(false));
        notification['success']({
            message: 'Permission Deleted!',
            description: `Permission ${payload.permission.title} deleted!`
        });
    } catch (e) {
        notification['error']({
            message: 'Failed Deleting Permission!',
            description: 'Failed deleting permission, please try again!'
        });
        console.log(e);
        yield put(permissionAction.permissionActionLoading(false));
    }
}

function* updatePermissionWatch({payload}) {
    try {
        yield put(permissionAction.permissionActionLoading(true));
        const permission = yield call(updatePermission, payload.id, payload.permission);
        yield put(permissionAction.updatePermissionSuccess(permission));
        notification['success']({
            message: 'Permission Updated!',
            description: `Permission ${permission.title} - ${permission.name} updated!`
        });
        yield put(permissionAction.permissionActionLoading(false));
        yield put(permissionAction.hideModalPermission());
    } catch (e) {
        notification['error']({
            message: 'Failed Updating Permission!',
            description: 'Failed updating permission, please try again!'
        });
        yield put(permissionAction.permissionActionLoading(false));
    }
}

function* getAllPermissionsWatch() {
    try {
        yield put(permissionAction.permissionPageLoading(true));
        const response = yield call(getAllPermissions);
        yield put(permissionAction.getAllPermissionSuccess(response));
        yield put(permissionAction.permissionPageLoading(false));
    } catch (e) {
        yield put(permissionAction.permissionPageLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(permissionAction.FIND_PERMISSION_REQUESTED, findCategoriesWatch),
        takeEvery(permissionAction.FIND_ALL_PARENT_PERMISSION_REQUESTED, findAllParentPermissionWatch),
        takeEvery(permissionAction.CREATE_NEW_PERMISSION_REQUESTED, createNewPermissionWatch),
        takeEvery(permissionAction.DELETE_PERMISSION_REQUESTED, deletePermissionWatch),
        takeEvery(permissionAction.UPDATE_PERMISSION_REQUESTED, updatePermissionWatch),
        takeEvery(permissionAction.GET_ALL_PERMISSION_REQUESTED, getAllPermissionsWatch),
    ]);
}
