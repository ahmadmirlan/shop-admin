import permissionAction from "./permission.action";
import {produce} from 'immer';

const initialState = {
    actionLoading: false,
    pageLoading: false,
    permissions: [],
    lastCreatedPermission: null,
    lastQuery: {},
    lastQueryResult: {},
    isModalOpen: false,
    dataLoaded: false,
    parentPermission: {data: [], isLoaded: false},
    allPermissions: {data: [], isLoaded: false}
}

export default function permissionReducer(state = initialState, {type, payload}) {
    switch (type) {
        case permissionAction.PERMISSION_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            }
        case permissionAction.PERMISSION_PAGE_LOADING:
            return {
                ...state,
                pageLoading: payload.isLoading
            }
        case permissionAction.FIND_PERMISSION_SUCCESS:
            return {
                ...state,
                permissions: payload.permission,
                lastQuery: payload.lastQuery,
                lastQueryResult: payload.lastQueryResult,
                dataLoaded: true
            }
        case permissionAction.FIND_ALL_PARENT_PERMISSION_SUCCESS:
            return {
                ...state,
                parentPermission: {data: payload.permission, isLoaded: true}
            }
        case permissionAction.CREATE_NEW_PERMISSION_SUCCESS:
            return {
                ...state,
                permissions: produce(state.permissions, draftState => {
                    draftState.unshift(payload.permission);
                }),
                parentPermission: produce(state.parentPermission, draftState => {
                    if (payload.permission.level === 1) {
                        draftState.data.unshift(payload.permission);
                    }
                }),
                allPermissions: produce(state.allPermissions, draftState => {
                    draftState.data.push(payload.permission);
                }),
                lastCreatedPermission: payload.permission
            }
        case permissionAction.DELETE_PERMISSION_SUCCESS:
            return {
                ...state,
                permissions: produce(state.permissions, draftState => {
                    const permissionIndex = draftState.findIndex(idx => idx.id === payload.permission.id);
                    if (permissionIndex > -1) {
                        draftState.splice(permissionIndex, 1);
                    }
                }),
                parentPermission: produce(state.parentPermission, draftState => {
                    const permissionIndex = draftState.data.findIndex(idx => idx.id === payload.permission.id);
                    if (permissionIndex > -1) {
                        draftState.data.splice(permissionIndex, 1);
                    }
                }),
                allPermissions: produce(state.allPermissions, draftState => {
                    const permissionIndex = draftState.data.findIndex(idx => idx.id === payload.permission.id);
                    if (permissionIndex > -1) {
                        draftState.data.splice(permissionIndex, 1);
                    }
                })
            }
        case permissionAction.UPDATE_PERMISSION_SUCCESS:
            return {
                ...state,
                permissions: produce(state.permissions, draftState => {
                    const permissionIndex = draftState.findIndex(idx => idx.id === payload.permission.id);
                    if (permissionIndex > -1) {
                        draftState[permissionIndex] = payload.permission;
                    }
                }),
                parentPermission: produce(state.parentPermission, draftState => {
                    const permissionIndex = draftState.data.findIndex(idx => idx.id === payload.permission.id);
                    if (permissionIndex > -1) {
                        draftState.data[permissionIndex] = payload.permission;
                    }
                }),
                allPermissions: produce(state.allPermissions, draftState => {
                    const permissionIndex = draftState.data.findIndex(idx => idx.id === payload.permission.id);
                    if (permissionIndex > -1) {
                        draftState.data[permissionIndex] = {
                            ...payload.permission,
                            parentId: payload.permission.parentId && payload.permission.parentId.id ? payload.permission.parentId.id : payload.permission.parentId
                        };
                    }
                })
            }
        case permissionAction.GET_ALL_PERMISSION_SUCCESS:
            return {
                ...state,
                allPermissions: {data: payload.permissions, isLoaded: true}
            }
        case permissionAction.OPEN_MODAL_PERMISSION:
            return {
                ...state,
                isModalOpen: true
            }
        case permissionAction.HIDE_MODAL_PERMISSION:
            return {
                ...state,
                isModalOpen: false
            }
        default:
            return {
                ...state
            }
    }
}
