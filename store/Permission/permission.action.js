const permissionAction = {
    PERMISSION_ACTION_LOADING: '[Permission] Permission Action Loading',
    PERMISSION_PAGE_LOADING: '[Permission] Permission Page Loading',
    FIND_PERMISSION_REQUESTED: '[Permission] Find Permission Requested',
    FIND_PERMISSION_SUCCESS: '[Permission] Find Permission Success',
    OPEN_MODAL_PERMISSION: '[Permission] Show Modal Permission',
    HIDE_MODAL_PERMISSION: '[Permission] Hide Modal Permission',
    FIND_ALL_PARENT_PERMISSION_REQUESTED: '[Permission] Find All Parent Permission Requested',
    FIND_ALL_PARENT_PERMISSION_SUCCESS: '[Permission] Find All Parent Permission Success',
    CREATE_NEW_PERMISSION_REQUESTED: '[Permission] Create New Permission Requested',
    CREATE_NEW_PERMISSION_SUCCESS: '[Permission] Create New Permission Success',
    DELETE_PERMISSION_REQUESTED: '[Permission] Delete Permission Requested',
    DELETE_PERMISSION_SUCCESS: '[Permission] Delete Permission Success',
    UPDATE_PERMISSION_REQUESTED: '[Permission] Update Permission Requested',
    UPDATE_PERMISSION_SUCCESS: '[Permission] Update Permission Success',
    GET_ALL_PERMISSION_REQUESTED: '[Permission] Get All Permission Requested',
    GET_ALL_PERMISSION_SUCCESS: '[Permission] Get All Permission Success',

    permissionActionLoading: (isLoading) => {
        return {
            type: permissionAction.PERMISSION_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    permissionPageLoading: (isLoading) => {
        return {
            type: permissionAction.PERMISSION_PAGE_LOADING,
            payload: {isLoading}
        }
    },

    findPermissionRequested: (query) => {
        return {
            type: permissionAction.FIND_PERMISSION_REQUESTED,
            payload: {query}
        }
    },

    findPermissionSuccess: (permission, lastQuery, lastQueryResult) => {
        return {
            type: permissionAction.FIND_PERMISSION_SUCCESS,
            payload: {permission, lastQuery, lastQueryResult}
        }
    },

    findAllParentPermissionRequested: () => {
        return {
            type: permissionAction.FIND_ALL_PARENT_PERMISSION_REQUESTED
        }
    },

    findAllParentPermissionSuccess: (permission) => {
        return {
            type: permissionAction.FIND_ALL_PARENT_PERMISSION_SUCCESS,
            payload: {permission}
        }
    },

    createNewPermissionRequested: (permission) => {
        return {
            type: permissionAction.CREATE_NEW_PERMISSION_REQUESTED,
            payload: {permission}
        }
    },

    createNewPermissionSuccess: (permission) => {
        return {
            type: permissionAction.CREATE_NEW_PERMISSION_SUCCESS,
            payload: {permission}
        }
    },

    deletePermissionRequested: (permission) => {
        return {
            type: permissionAction.DELETE_PERMISSION_REQUESTED,
            payload: {permission}
        }
    },

    deletePermissionSuccess: (permission) => {
        return {
            type: permissionAction.DELETE_PERMISSION_SUCCESS,
            payload: {permission}
        }
    },

    updatePermissionRequested: (id, permission) => {
        return {
            type: permissionAction.UPDATE_PERMISSION_REQUESTED,
            payload: {id, permission}
        }
    },

    updatePermissionSuccess: (permission) => {
        return {
            type: permissionAction.UPDATE_PERMISSION_SUCCESS,
            payload: {permission}
        }
    },

    openModalPermission: () => {
        return {
            type: permissionAction.OPEN_MODAL_PERMISSION
        }
    },

    hideModalPermission: () => {
        return {
            type: permissionAction.HIDE_MODAL_PERMISSION
        }
    },

    getAllPermissionRequested: () => {
        return {
            type: permissionAction.GET_ALL_PERMISSION_REQUESTED
        }
    },

    getAllPermissionSuccess: (permissions) => {
        return {
            type: permissionAction.GET_ALL_PERMISSION_SUCCESS,
            payload: {permissions}
        }
    }
}
export default permissionAction;
