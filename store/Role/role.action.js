const roleAction = {
    ROLE_ACTION_LOADING: '[Role] Action Loading',
    ROLE_PAGE_LOADING: '[Role] Page Loading',
    FIND_ROLE_REQUESTED: '[Role] Find Role Requested',
    FIND_ROLE_SUCCESS: '[Role] Find Role Success',
    OPEN_ROLE_MODAL: '[Role] Open Role Modal',
    HIDE_ROLE_MODAL: '[Role] Hide Role Modal',
    UPDATE_ROLE_REQUESTED: '[Role] Update Role Requested',
    UPDATE_ROLE_SUCCESS: '[Role] Update Role Success',
    CREATE_ROLE_REQUESTED: '[Role] Create Role Requested',
    CREATE_ROLE_SUCCESS: '[Role] Create Role Success',
    GET_ALL_ROLES_REQUESTED: '[Role] Get All Roles Requested',
    GET_ALL_ROLES_SUCCESS: '[Role] Get All Roles Success',

    roleActionLoading: (isLoading) => {
        return {
            type: roleAction.ROLE_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    rolePageLoading: (isLoading) => {
        return {
            type: roleAction.ROLE_PAGE_LOADING,
            payload: {isLoading}
        }
    },

    findRoleRequested: (query) => {
        return {
            type: roleAction.FIND_ROLE_REQUESTED,
            payload: {query}
        }
    },

    findRoleSuccess: (roles, lastQuery, lastQueryResult) => {
        return {
            type: roleAction.FIND_ROLE_SUCCESS,
            payload: {roles, lastQuery, lastQueryResult}
        }
    },

    updateRoleRequested: (id, role) => {
        return {
            type: roleAction.UPDATE_ROLE_REQUESTED,
            payload: {id, role}
        }
    },

    updateRoleSuccess: (role) => {
        return {
            type: roleAction.UPDATE_ROLE_SUCCESS,
            payload: {role}
        }
    },

    createRoleRequested: (role) => {
        return {
            type: roleAction.CREATE_ROLE_REQUESTED,
            payload: {role}
        }
    },

    createRoleSuccess: (role) => {
        return {
            type: roleAction.CREATE_ROLE_SUCCESS,
            payload: {role}
        }
    },

    openRoleModal: () => {
        return {
            type: roleAction.OPEN_ROLE_MODAL
        }
    },

    hideRoleModal: () => {
        return {
            type: roleAction.HIDE_ROLE_MODAL
        }
    },

    getAllRoleRequested: () => {
        return {
            type: roleAction.GET_ALL_ROLES_REQUESTED
        }
    },

    getAllRolesSuccess: (roles) => {
        return {
            type: roleAction.GET_ALL_ROLES_SUCCESS,
            payload: {roles}
        }
    }
}

export default roleAction;
