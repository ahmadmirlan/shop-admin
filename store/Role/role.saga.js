import {all, call, put, takeEvery} from 'redux-saga/effects'
import roleAction from "./role.action";
import {createRole, findRoles, getAllRoles, updateRole} from "../../helper/role.helper";
import {notification} from "antd";


function* findRolesWatch({payload}) {
    try {
        yield put(roleAction.rolePageLoading(true));
        const response = yield call(findRoles, payload.query);
        yield put(roleAction.findRoleSuccess(response.data, payload.query, response.page));
        yield put(roleAction.rolePageLoading(false));
    } catch (e) {
        yield put(roleAction.rolePageLoading(false));
    }
}

function* updateRoleWatch({payload}) {
    try {
        yield put(roleAction.roleActionLoading(true));
        const response = yield call(updateRole, payload.id, payload.role);
        yield put(roleAction.updateRoleSuccess(response));
        notification['success']({
            message: 'Role Updated!',
            description: `Role "${response.title}" updated!`
        });
        yield put(roleAction.roleActionLoading(false));
        yield put(roleAction.hideRoleModal());
    } catch (e) {
        notification['error']({
            message: 'Failed Updating Role!',
            description: 'Failed updating role, please try again!'
        });
        yield put(roleAction.roleActionLoading(false));
    }
}

function* createRoleWatch({payload}) {
    try {
        yield put(roleAction.roleActionLoading(true));
        const role = yield call(createRole, payload.role);
        yield put(roleAction.createRoleSuccess(role));
        notification['success']({
            message: 'Role Added!',
            description: `Role "${role.title}" added!`
        });
        yield put(roleAction.roleActionLoading(false));
        yield put(roleAction.hideRoleModal());
    } catch (e) {
        notification['error']({
            message: 'Failed Creating Role!',
            description: 'Failed creating role, please try again!'
        });
        yield put(roleAction.roleActionLoading(false));
    }
}

function* getAllRolesWatch() {
    try {
        yield put(roleAction.rolePageLoading(true));
        const resp = yield call(getAllRoles);
        yield put(roleAction.getAllRolesSuccess(resp));
        yield put(roleAction.rolePageLoading(false));
    } catch (e) {
        yield put(roleAction.rolePageLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(roleAction.FIND_ROLE_REQUESTED, findRolesWatch),
        takeEvery(roleAction.UPDATE_ROLE_REQUESTED, updateRoleWatch),
        takeEvery(roleAction.CREATE_ROLE_REQUESTED, createRoleWatch),
        takeEvery(roleAction.GET_ALL_ROLES_REQUESTED, getAllRolesWatch),
    ]);
}
