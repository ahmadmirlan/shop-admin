import roleAction from "./role.action";
import produce from "immer";

const initialState = {
    actionLoading: false,
    pageLoading: false,
    roles: [],
    dataLoaded: false,
    lastCreatedRole: null,
    lastQuery: {},
    lastQueryResult: {},
    isModalOpen: false,
    allRole: {data: [], isLoaded: false}
}

export default function roleReducer(state = initialState, {type, payload}) {
    switch (type) {
        case roleAction.ROLE_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            }
        case roleAction.ROLE_PAGE_LOADING:
            return {
                ...state,
                pageLoading: payload.isLoading
            }
        case roleAction.FIND_ROLE_SUCCESS:
            return {
                ...state,
                roles: payload.roles,
                dataLoaded: true,
                lastQueryResult: payload.lastQueryResult,
                lastQuery: payload.lastQuery
            }
        case roleAction.UPDATE_ROLE_SUCCESS:
            return {
                ...state,
                roles: produce(state.roles, draftState => {
                    const findIndex = draftState.findIndex(role => role.id === payload.role.id);
                    if (findIndex > -1) {
                        draftState[findIndex] = payload.role
                    }
                })
            }
        case roleAction.CREATE_ROLE_SUCCESS:
            return {
                ...state,
                roles: produce(state.roles, draftState => {
                    draftState.unshift(payload.role);
                })
            }
        case roleAction.OPEN_ROLE_MODAL:
            return {
                ...state,
                isModalOpen: true
            }
        case roleAction.HIDE_ROLE_MODAL:
            return {
                ...state,
                isModalOpen: false
            }
        case roleAction.GET_ALL_ROLES_SUCCESS:
            return {
                ...state,
                allRole: {data: payload.roles, isLoaded: true}
            }
        default:
            return {
                ...state
            }
    }
}
