import {all, call, put, takeEvery} from 'redux-saga/effects'
import authAction from "./auth.action";
import {login} from "../../helper/auth.helper";
import {message} from "antd";

function* loginWatch({payload}) {
    try {
        yield put(authAction.loginActionLoading(true));
        const user = yield call(login, payload.login);
        yield put(authAction.loginSuccess(user));
        yield put(authAction.authCheckExistingLoginSuccess(user));
        localStorage.setItem('auth_token_user', user.accessToken ? user.accessToken : null);
        localStorage.setItem('user', JSON.stringify(user));
        message.success('Login success!');
        yield put(authAction.loginActionLoading(false));
    } catch (e) {
        message.error('Invalid email/password');
        yield put(authAction.loginActionLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(authAction.LOGIN_REQUESTED, loginWatch)
    ])
}
