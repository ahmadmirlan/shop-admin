const authAction = {
    LOGIN_ACTION_LOADING: '[Login] Action Loading',
    LOGIN_REQUESTED: '[Login] Login Requested',
    LOGIN_SUCCESS: '[Login] Success',
    AUTH_CHECK_EXISTING_LOGIN_SUCCESS: '[Auth] Check Existing Login Success',
    AUTH_LOGOUT: '[Auth] Auth Logout',

    loginActionLoading: (isLoading) => {
        return {
            type: authAction.LOGIN_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    loginRequested: (login) => {
        return {
            type: authAction.LOGIN_REQUESTED,
            payload: {login}
        }
    },

    loginSuccess: (user) => {
        return {
            type: authAction.LOGIN_SUCCESS,
            payload: {user}
        }
    },

    authCheckExistingLoginSuccess: (user) => {
        return {
            type: authAction.AUTH_CHECK_EXISTING_LOGIN_SUCCESS,
            payload: {user}
        }
    },

    authLogout: () => {
        return {
            type: authAction.AUTH_LOGOUT
        }
    }
};

export default authAction;
