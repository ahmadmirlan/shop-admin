import authAction from "./auth.action";

const initialState = {
    actionLoading: false,
    isLogged: false,
    user: null
}

export default function authReducer(state = initialState, {type, payload}) {
    switch (type) {
        case authAction.LOGIN_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            }
        case authAction.LOGIN_SUCCESS:
            return {
                ...state,
                isLogged: true,
                user: payload.user
            }
        case authAction.AUTH_CHECK_EXISTING_LOGIN_SUCCESS:
            return {
                ...state,
                isLogged: true,
                user: payload.user
            }
        case authAction.AUTH_LOGOUT:
            localStorage.removeItem('auth_token_user');
            localStorage.removeItem('user');
            return {
                ...state,
                user: null,
                isLogged: false
            }
        default:
            return {
                ...state
            }
    }
}
