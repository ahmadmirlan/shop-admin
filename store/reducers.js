import {combineReducers} from 'redux';
import Category from './Category/category.reducer';
import MasterCategory from './MasterCategory/master-category.reducer';
import Permission from './Permission/permission.reducer';
import Role from './Role/role.reducer';
import Auth from './Auth/auth.reducer';
import User from './User/user.reducer';

const rootReducer = combineReducers({
    Category,
    MasterCategory,
    Permission,
    Role,
    Auth,
    User
});

export default rootReducer;
