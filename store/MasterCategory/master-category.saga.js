import {all, call, put, takeEvery} from "redux-saga/effects";
import masterCategoryAction from "./master-category.action";
import {
    findAllMasterCategory,
    createNewMasterCategory,
    removeMasterCategory, updateMasterCategory, loadAllMasterCategories
} from "../../helper/master-category.helper";
import {notification} from "antd";

function* loadMasterCategoryWatch({payload}) {
    try {
        yield put(masterCategoryAction.masterCategoryPageLoading(true));
        const response = yield call(findAllMasterCategory, payload.query);
        yield put(masterCategoryAction.loadAllMasterCategorySuccess(response.data, payload.query, response.page));
        yield put(masterCategoryAction.masterCategoryPageLoading(false));
    } catch (e) {
        yield put(masterCategoryAction.masterCategoryPageLoading(false));
    }
}

function* createNewMasterCategoryWatch({payload}) {
    try {
        yield put(masterCategoryAction.masterCategoryActionLoading(true));
        const response = yield call(createNewMasterCategory, payload.masterCategory);
        yield put(masterCategoryAction.createMasterCategorySuccess(response));
        notification['success']({
            message: 'Master Category Created',
            description:
                `New master category "${payload.masterCategory.name}" created!`,
        });
        yield put(masterCategoryAction.masterCategoryActionLoading(false));
        yield put(masterCategoryAction.hideModalCreateCategory());
    } catch (e) {
        yield put(masterCategoryAction.masterCategoryActionLoading(false))
        notification['error']({
            message: 'Failed Creating Master Category',
            description:
                `Failed creating master category, please make sure "${payload.masterCategory.name}" is not duplicated!`,
        });
    }
}

function* removeCategoryMasterWatch({payload}) {
    try {
        yield put(masterCategoryAction.masterCategoryActionLoading(true));
        yield call(removeMasterCategory, payload.masterCategory.id);
        yield put(masterCategoryAction.removeCategorySuccess(payload.masterCategory));
        notification['success']({
            message: 'Master Category Deleted',
            description:
                `Master category "${payload.masterCategory.name}" deleted!`,
        });
        yield put(masterCategoryAction.masterCategoryActionLoading(false));
    } catch (e) {
        console.log(e);
        notification['error']({
            message: 'Failed Removing Master Category',
            description:
                `Failed removing master category, please make sure "${payload.masterCategory.name}" is not used by any category!`,
        });
        yield put(masterCategoryAction.masterCategoryActionLoading(false));
    }
}

function* updateMasterCategoryWatch({payload}) {
    try {
        yield put(masterCategoryAction.masterCategoryActionLoading(true));
        const response = yield call(updateMasterCategory, payload.masterCategory);
        yield put(masterCategoryAction.updateMasterCategorySuccess(response));
        notification['success']({
            message: 'Master Category Updated!',
            description:
                `Master category "${payload.masterCategory.name}" updated!`,
        });
        yield put(masterCategoryAction.masterCategoryActionLoading(false));
        yield put(masterCategoryAction.hideModalCreateCategory());
    } catch (e) {
        notification['error']({
            message: 'Failed Updating Master Category',
            description:
                `Failed updating master category, please make sure "${payload.masterCategory.name}" is not duplicated!`,
        });
        yield put(masterCategoryAction.masterCategoryActionLoading(false));
    }
}

function* loadAllMasterCategoriesWatch() {
    try {
        yield put(masterCategoryAction.masterCategoryPageLoading(true));
        const response = yield call(loadAllMasterCategories);
        yield put(masterCategoryAction.findAllMasterCategoriesSuccess(response));
        yield put(masterCategoryAction.masterCategoryPageLoading(false));
    } catch (e) {
        yield put(masterCategoryAction.masterCategoryPageLoading(false));
    }
}

export default function* rootSaga() {
    yield all([
        takeEvery(masterCategoryAction.LOAD_ALL_MASTER_CATEGORY_REQUESTED, loadMasterCategoryWatch),
        takeEvery(masterCategoryAction.CREATE_NEW_MASTER_CATEGORY_REQUESTED, createNewMasterCategoryWatch),
        takeEvery(masterCategoryAction.REMOVE_MASTER_CATEGORY_REQUESTED, removeCategoryMasterWatch),
        takeEvery(masterCategoryAction.UPDATE_MASTER_CATEGORY_REQUESTED, updateMasterCategoryWatch),
        takeEvery(masterCategoryAction.FIND_ALL_MASTER_CATEGORIES_REQUESTED, loadAllMasterCategoriesWatch)
    ])
}
