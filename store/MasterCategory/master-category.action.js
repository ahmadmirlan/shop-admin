const masterCategoryAction = {
    /*Loading*/
    MASTER_CATEGORY_ACTION_LOADING: '[Master Category] Action Loading',
    MASTER_CATEGORY_PAGE_LOADING: '[Master Category] Page Action',
    /*Create new master category*/
    CREATE_NEW_MASTER_CATEGORY_REQUESTED: '[Master Category] Create New Category Requested',
    CREATE_NEW_MASTER_CATEGORY_SUCCESS: '[Master Category] Create New Category Success',
    LOAD_ALL_MASTER_CATEGORY_REQUESTED: '[Master Category] Load All Category Requested',
    LOAD_ALL_MASTER_CATEGORY_SUCCESS: '[Master Category] Load All Category Success',
    FIND_ALL_MASTER_CATEGORIES_REQUESTED: '[Master Categories] Find All Master Categories Requested',
    FIND_ALL_MASTER_CATEGORIES_SUCCESS: '[Master Categories] Find All Master Categories Success',
    REMOVE_MASTER_CATEGORY_REQUESTED: '[Master Category] Remove Category Requested',
    REMOVE_MASTER_CATEGORY_SUCCESS: '[Master Category] Remove Category Success',
    UPDATE_MASTER_CATEGORY_REQUESTED: '[Master Category] Update Master Category Requested',
    UPDATE_MASTER_CATEGORY_SUCCESS: '[Master Category] Update Master Category Success',
    OPEN_MODAL_CREATE_CATEGORY: '[Master Category] Open Modal Create Category',
    HIDE_MODAL_CREATE_CATEGORY: '[Master Category] Hide Modal Create Category',

    masterCategoryActionLoading: (isLoading) => {
        return {
            type: masterCategoryAction.MASTER_CATEGORY_ACTION_LOADING,
            payload: {isLoading}
        }
    },

    masterCategoryPageLoading: (isLoading) => {
        return {
            type: masterCategoryAction.MASTER_CATEGORY_PAGE_LOADING,
            payload: {isLoading}
        }
    },

    createMasterCategoryRequested: (masterCategory) => {
        return {
            type: masterCategoryAction.CREATE_NEW_MASTER_CATEGORY_REQUESTED,
            payload: {masterCategory}
        }
    },

    createMasterCategorySuccess: (masterCategory) => {
        return {
            type: masterCategoryAction.CREATE_NEW_MASTER_CATEGORY_SUCCESS,
            payload: {masterCategory}
        }
    },

    loadAllMasterCategoryRequested: (query) => {
        return {
            type: masterCategoryAction.LOAD_ALL_MASTER_CATEGORY_REQUESTED,
            payload: {query}
        }
    },

    loadAllMasterCategorySuccess: (masterCategories, lastQuery, lastQueryResult) => {
        return {
            type: masterCategoryAction.LOAD_ALL_MASTER_CATEGORY_SUCCESS,
            payload: {masterCategories, lastQuery, lastQueryResult}
        }
    },

    removeCategoryRequested: (masterCategory) => {
        return {
            type: masterCategoryAction.REMOVE_MASTER_CATEGORY_REQUESTED,
            payload: {masterCategory}
        }
    },

    removeCategorySuccess: (masterCategory) => {
        return {
            type: masterCategoryAction.REMOVE_MASTER_CATEGORY_SUCCESS,
            payload: {masterCategory}
        }
    },

    updateMasterCategoryRequested: (masterCategory) => {
        return {
            type: masterCategoryAction.UPDATE_MASTER_CATEGORY_REQUESTED,
            payload: {masterCategory}
        }
    },

    updateMasterCategorySuccess: (masterCategory) => {
        return {
            type: masterCategoryAction.UPDATE_MASTER_CATEGORY_SUCCESS,
            payload: {masterCategory}
        }
    },

    findAllMasterCategoriesRequested: () => {
        return {
            type: masterCategoryAction.FIND_ALL_MASTER_CATEGORIES_REQUESTED,
        }
    },

    findAllMasterCategoriesSuccess: (masterCategories) => {
        return {
            type: masterCategoryAction.FIND_ALL_MASTER_CATEGORIES_SUCCESS,
            payload: {masterCategories}
        }
    },

    openModalCreateCategory: () => {
        return {
            type: masterCategoryAction.OPEN_MODAL_CREATE_CATEGORY
        }
    },

    hideModalCreateCategory: () => {
        return {
            type: masterCategoryAction.HIDE_MODAL_CREATE_CATEGORY
        }
    },
}

export default masterCategoryAction;
