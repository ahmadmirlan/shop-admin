import masterCategoryAction from "./master-category.action";
import produce from "immer";

const initialState = {
    actionLoading: false,
    pageLoading: false,
    masterCategories: [],
    allMasterCategories: {data: [], isLoaded: false},
    lastCreatedMasterCategory: null,
    lastQuery: {},
    lastQueryResult: {},
    isModalOpen: false,
}

export default function categoryReducer(state = initialState, {type, payload}) {
    switch (type) {
        case masterCategoryAction.MASTER_CATEGORY_PAGE_LOADING:
            return {
                ...state,
                pageLoading: payload.isLoading
            };
        case masterCategoryAction.MASTER_CATEGORY_ACTION_LOADING:
            return {
                ...state,
                actionLoading: payload.isLoading
            }
        case masterCategoryAction.LOAD_ALL_MASTER_CATEGORY_SUCCESS:
            return {
                masterCategories: payload.masterCategories,
                lastQuery: payload.lastQuery,
                lastQueryResult: payload.lastQueryResult,
            }
        case masterCategoryAction.OPEN_MODAL_CREATE_CATEGORY:
            return {
                ...state,
                isModalOpen: true
            }
        case masterCategoryAction.HIDE_MODAL_CREATE_CATEGORY:
            return {
                ...state,
                isModalOpen: false
            }
        case masterCategoryAction.CREATE_NEW_MASTER_CATEGORY_SUCCESS:
            return {
                ...state,
                lastCreatedMasterCategory: payload.masterCategory,
                masterCategories: produce(state.masterCategories, draftState => {
                    draftState.unshift(payload.masterCategory);
                }),
            }
        case masterCategoryAction.REMOVE_MASTER_CATEGORY_SUCCESS:
            return {
                ...state,
                masterCategories: produce(state.masterCategories, draftState => {
                    const categoryIndex = draftState.findIndex(cat => cat.id === payload.masterCategory.id);
                    if (categoryIndex > -1) {
                        draftState.splice(categoryIndex, 1);
                    }
                }),
            }
        case masterCategoryAction.UPDATE_MASTER_CATEGORY_SUCCESS:
            return {
                ...state,
                masterCategories: produce(state.masterCategories, draftState => {
                    const categoryIndex = draftState.findIndex(cat => cat.id === payload.masterCategory.id);
                    if (categoryIndex > -1) {
                        draftState[categoryIndex] = payload.masterCategory;
                    }
                }),
            }
        case masterCategoryAction.FIND_ALL_MASTER_CATEGORIES_SUCCESS:
            return {
                ...state,
                allMasterCategories: {data: payload.masterCategories, isLoaded: true}
            }
        default:
            return {
                ...state
            }
    }
}
