import {all} from 'redux-saga/effects'
import Category from './Category/category.saga';
import MasterCategory from './MasterCategory/master-category.saga';
import Permission from './Permission/permission.saga';
import Role from './Role/role.saga';
import Auth from './Auth/auth.saga';
import User from './User/user.saga';

export default function* rootSaga() {
    yield all([Auth(), Category(), MasterCategory(), Permission(), Role(), User()])
}
