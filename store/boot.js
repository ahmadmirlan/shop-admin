import {getCurrentToken, validateToken} from '../helper/auth.helper';
import authAction from './Auth/auth.action';
import {store} from './store';

export default () =>
    new Promise(() => {
        if (typeof window !== "undefined") {
            const token = getCurrentToken();
            if (token !== '') {
                validateToken().then(resp => {
                    if (resp) {
                        store.dispatch(authAction.authCheckExistingLoginSuccess(resp));
                    }
                }).catch(() => {
                    store.dispatch(authAction.authLogout());
                    localStorage.removeItem('auth_token_user');
                    localStorage.removeItem('user');
                });
            } else {
                store.dispatch(authAction.authLogout());
            }
        }
    });
