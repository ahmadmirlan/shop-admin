import UserPage from "./presentational/UserPage";
import {useEffect} from 'react';

export default function User() {
    useEffect(() => {
        document.title = 'User Pages'
    });
    return (
        <div className="flex-1 px-4 py-2 bg-white rounded-xl h-auto">
            <UserPage/>
        </div>
    )
}
