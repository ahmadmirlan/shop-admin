import {Button, Input, Modal, Select, Tag} from "antd";
import {useFormik} from "formik";
import {useDispatch} from "react-redux";
import userAction from "../../../../store/User/user.action";
import {userSchema} from "../../schema/user-schema";
import {CheckOutlined, StopOutlined, LoadingOutlined} from '@ant-design/icons';
import {useEffect} from 'react';

const {Option} = Select;

export default function AddUserDialog(props) {
    const dispatch = useDispatch();
    const userForm = useFormik({
        validationSchema: userSchema,
        initialValues: {
            firstName: '',
            lastName: '',
            username: '',
            email: '',
            role: '',
            status: 'PENDING'
        },
        onSubmit: values => {
            if (props.user) {
                dispatch(userAction.updateUserRequested(values, props.user.id));
            } else {
                dispatch(userAction.createNewUserRequested(values));
            }
        }
    });

    const onEmailChanges = (value) => {
        if (!userForm.errors.email) {
            dispatch(userAction.checkEmailRequested(value.target.value));
        }
    }

    const onUsernameChanges = (value) => {
        if (!userForm.errors.username) {
            dispatch(userAction.checkUsernameRequested(value.target.value));
        }
    }

    useEffect(() => {
        userForm.setValues({
            firstName: props.user ? props.user.firstName : '',
            lastName: props.user ? props.user.lastName : '',
            username: props.user ? props.user.username : '',
            email: props.user ? props.user.email : '',
            role: props.user ? props.user.role.id : '',
            status: props.user ? props.user.status : 'PENDING'
        });
    }, [props.user])

    return (
        <Modal
            visible={props.isModalOpen}
            title={props.user ? `Update User - @${props.user.username}` : 'Add User'}
            onOk={() => props.showModal(false)}
            onCancel={() => props.showModal(false)}
            footer={[
                <Button key="back" onClick={() => props.showModal(false)}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" onClick={userForm.handleSubmit} loading={props.actionLoading}
                        disabled={!userForm.isValid || props.validateEmail.status || props.validateUsername.status}>
                    Save
                </Button>,
            ]}
        >
            <Input placeholder="First Name" size="large" className="rounded-md" name="firstName" id="firstName"
                   value={userForm.values.firstName} onChange={userForm.handleChange}/>
            <div className="flex">
                <Input placeholder="Last Name" size="large" className="flex-1 rounded-md mt-4" name="lastName"
                       id="lastName"
                       value={userForm.values.lastName} onChange={userForm.handleChange}/>
            </div>
            <Input placeholder="Username" size="large" className="rounded-md mt-4" name="username" id="username"
                   value={userForm.values.username} onChange={userForm.handleChange}
                   suffix={usernameValidation(props.validateUsername, !userForm.errors.username)}
                   onBlur={value => onUsernameChanges(value)} disabled={props.user}/>
            <Input placeholder="Email" type="email" size="large" className="rounded-md mt-4" name="email" id="email"
                   value={userForm.values.email} onChange={userForm.handleChange}
                   suffix={emailValidation(props.validateEmail, !userForm.errors.email)}
                   onBlur={value => onEmailChanges(value)} disabled={props.user}/>
            <div className="flex mt-4">
                <Select
                    size="large"
                    placeholder="Select role"
                    name="role"
                    className="w-full mr-4"
                    value={userForm.values.role} onChange={value => userForm.setFieldValue('role', value)}
                >
                    {props.roles.map(role => {
                        return (<Option value={role.id} key={role.id}>{role.title}</Option>)
                    })}
                </Select>
                <Select
                    size="large"
                    placeholder="Select status"
                    name="status"
                    className="w-full"
                    disabled={!props.user}
                    onChange={value => userForm.setFieldValue('status', value)}
                    value={userForm.values.status}
                >
                    <Option value="ACTIVE" key="ACTIVE">
                        <Tag color="lime">ACTIVE</Tag>
                    </Option>
                    <Option value="INACTIVE" key="INACTIVE">
                        <Tag color="orange">INACTIVE</Tag>
                    </Option>
                    <Option value="DELETED" key="DELETED">
                        <Tag color="red">DELETED</Tag>
                    </Option>
                    <Option value="PENDING" key="PENDING">
                        <Tag color="gold">PENDING</Tag>
                    </Option>
                </Select>
            </div>
        </Modal>
    );
}

const emailValidation = (validateEmail, isFormValid) => {
    if (!validateEmail.status && !validateEmail.loading && isFormValid) {
        return (<span className="flex items-center p-1 rounded-full bg-green-300"><CheckOutlined
            style={{color: '#ffff'}}/></span>)
    } else if (validateEmail.loading) {
        return (<span className="flex items-center p-1 rounded-full bg-gray-500"><LoadingOutlined
            style={{color: '#ffff'}}/></span>)
    } else {
        return (<span className="flex items-center justify-center p-1 rounded-full bg-red-300"><StopOutlined
            style={{color: '#ffff', width: '100%', height: '100%'}}/></span>)
    }
}

const usernameValidation = (validateUsername, isValid) => {
    if (!validateUsername.status && !validateUsername.loading && isValid) {
        return (<span className="flex items-center p-1 rounded-full bg-green-300"><CheckOutlined
            style={{color: '#ffff'}}/></span>)
    } else if (validateUsername.loading) {
        return (<span className="flex items-center p-1 rounded-full bg-gray-500"><LoadingOutlined
            style={{color: '#ffff'}}/></span>)
    } else {
        return (<span className="flex items-center justify-center p-1 rounded-full bg-red-300"><StopOutlined
            style={{color: '#ffff', width: '100%', height: '100%'}}/></span>)
    }
}
