import UserTable from "./UserTable";
import {useDispatch, useSelector} from "react-redux";
import {Fragment, useEffect} from 'react';
import userAction from "../../../../store/User/user.action";
import {Button} from "antd";
import AddUserDialog from "../dialog/AddUserDialog";
import roleAction from "../../../../store/Role/role.action";
import {useState} from 'react';

export default function UserPage() {
    const dispatch = useDispatch();
    const [user, setUser] = useState(null);
    const {
        pageLoading,
        actionLoading,
        users,
        dataLoaded,
        lastQueryResult,
        queryResult,
        isModalOpen,
        validateEmail,
        validateUsername
    } = useSelector(state => state.User);

    const {allRole} = useSelector(state => state.Role);

    let query = {
        "filter": {},
        "pageNumber": 0,
        "pageSize": 5,
        "sortField": "",
        "sortOrder": "desc"
    }
    const onSizeChange = (current, size) => {
        query.pageNumber = 0;
        query.pageSize = size;
    };
    const paginationChange = (page, pageSize) => {
        query.pageNumber = (page - 1);
        query.pageSize = pageSize;
    };

    const showModal = (isShow) => {
        if (isShow) {
            setUser(null);
            dispatch(userAction.openUserModal());
        } else {
            dispatch(userAction.hideUserModal());
        }
    }

    const onUpdate = (user) => {
        setUser(user);
        dispatch(userAction.openUserModal());
    };

    const onDelete = (user) => {
        console.log(user);
    }

    useEffect(() => {
        if (!dataLoaded) {
            dispatch(userAction.findUsersRequested(query));
        }
        if (!allRole.isLoaded) {
            dispatch(roleAction.getAllRoleRequested());
        }
    }, [dataLoaded, allRole])

    return (
        <Fragment>
            <HeaderTitle showModal={showModal}/>
            <UserTable dataSource={users} pageLoading={pageLoading} queryResult={lastQueryResult}
                       sizeChange={onSizeChange}
                       paginationChange={paginationChange} onUpdate={onUpdate} onDelete={onDelete}/>
            <AddUserDialog isModalOpen={isModalOpen} showModal={showModal} roles={allRole?.data}
                           actionLoading={actionLoading} validateEmail={validateEmail}
                           validateUsername={validateUsername} user={user}/>
        </Fragment>
    )
}

const HeaderTitle = (props) => {
    return (
        <div className="flex py-4 px-2 items-center justify-between">
            <Button type="primary" onClick={() => props.showModal(true)}>Add Data</Button>
        </div>
    );
};
