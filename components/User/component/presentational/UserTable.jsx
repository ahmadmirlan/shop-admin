import {Button, Space, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";
import CustomTable from "../../../shared/components/CustomTable";

export default function UserTable(props) {
    const userTableHeader = [
        {
            title: 'First Name',
            dataIndex: 'firstName',
            key: 'id',
            render: text => <p>{text}</p>,
        },
        {
            title: 'Last Name',
            dataIndex: 'lastName',
            key: 'id',
            render: text => <p>{text}</p>
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
            render: text => <p>@{text}</p>
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
            render: text => <span>{text}</span>
        },
        {
            title: 'Role',
            dataIndex: 'role',
            key: 'id',
            render: role => <span>{role?.title}</span>,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'id',
            render: status => (
                <Tag color="lime">{status}</Tag>
            )
        },
        {
            title: 'Action',
            key: 'id',
            render: (text, record) => (
                !record.isCoreUser ? <Space size="middle">
                    <Tooltip title="Edit">
                        <Button size="small" type="dashed" shape="circle" icon={<EditOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onUpdate(record)}/>
                    </Tooltip>
                    <Tooltip title="Delete">
                        <Button size="small" type="dashed" danger shape="circle" icon={<DeleteOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onDelete(record)}/>
                    </Tooltip>
                </Space> : <></>
            ),
        },
    ];
    return (
        <CustomTable dataSource={props.dataSource} columns={userTableHeader} pageLoading={props.pageLoading}
                     queryResult={props.queryResult} sizeChange={props.sizeChange}
                     paginationChange={props.paginationChange}/>
    );
}
