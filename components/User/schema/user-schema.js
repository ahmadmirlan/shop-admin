import * as Yup from 'yup';

export const userSchema = Yup.object().shape({
    firstName: Yup.string().required('Please provide first name'),
    lastName: Yup.string(),
    username: Yup.string().min(6, 'Username at least 6 character').required('Please provide username'),
    email: Yup.string().email('Please provide valid email').required('PLease provide an email'),
    role: Yup.string().required('Please select role')
});
