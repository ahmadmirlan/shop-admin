import {Button, Input, Modal, Select, Tag} from "antd";
import {useFormik} from "formik";
import {permissionSchema} from "../../schema/permission-schema";
import {useDispatch} from "react-redux";
import permissionAction from "../../../../store/Permission/permission.action";
import {useEffect} from 'react';

const {Option} = Select;

export default function AddPermissionDialog(props) {
    const dispatch = useDispatch();
    const permissionForm = useFormik({
        validationSchema: permissionSchema,
        initialValues: {
            title: '',
            name: '',
            level: 2,
            parentId: ''
        },
        onSubmit: values => {
            if (props.permission) {
                dispatch(permissionAction.updatePermissionRequested(props.permission.id, {
                    ...values,
                    parentId: values.parentId === '' ? null : values.parentId
                }))
            } else {
                dispatch(permissionAction.createNewPermissionRequested({
                    ...values,
                    parentId: values.parentId === '' ? null : values.parentId
                }));
            }
        }
    });

    const isFormValid = () => {
        return permissionForm.values.level === 1 || (permissionForm.values.level === 2 && permissionForm.values.parentId !== '');
    }

    useEffect(() => {
        permissionForm.setValues({
            title: props.permission ? props.permission.title : '',
            name: props.permission ? props.permission.name : '',
            level: props.permission ? props.permission.level : 2,
            parentId: props.permission && props.permission.parentId ? props.permission.parentId.id : ''
        });
    }, [props.permission])

    return (
        <Modal
            visible={props.isModalOpen}
            title={props.permission ? `Update Permission - ${props.permission.name}` : 'Add Permission'}
            onOk={() => props.showModal(false)}
            onCancel={() => props.showModal(false)}
            footer={[
                <Button key="back" onClick={() => props.showModal(false)}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={props.actionLoading}
                        disabled={!permissionForm.isValid || !isFormValid()}
                        onClick={permissionForm.handleSubmit}>
                    Save
                </Button>,
            ]}
        >
            <form onSubmit={permissionForm.handleSubmit}>
                <Input placeholder="Title" size="large" className="rounded-md" name="title" id="title"
                       onChange={permissionForm.handleChange} value={permissionForm.values.title}/>
                <Input placeholder="Name" size="large" className="rounded-md mt-4" name="name" id="name"
                       onChange={permissionForm.handleChange} value={permissionForm.values.name}/>
                <Select
                    size="large"
                    placeholder="Select level"
                    name="level"
                    className="mt-4 w-full"
                    onChange={value => {
                        if (Number(value) === 1) {
                            permissionForm.setFieldValue('parentId', '');
                        }
                        permissionForm.setFieldValue('level', Number(value));
                    }}
                    id="level" value={permissionForm.values.level}>
                    <Option key="1" value="1" id="1"><Tag color="red">1</Tag></Option>
                    <Option key="2" value="2" id="2"><Tag color="green">2</Tag></Option>
                </Select>
                {
                    permissionForm.values.level === 2 ? <Select
                        size="large"
                        showSearch
                        placeholder="Please select"
                        optionFilterProp="name"
                        name="parentId"
                        id="parentId"
                        onChange={value => permissionForm.setFieldValue('parentId', value)}
                        value={permissionForm.values.parentId}
                        filterOption={(input, option) =>
                            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                        className="mt-4 w-full rounded-md"
                    >
                        {props.parent ? props.parent.map(perm => {
                            return (<Option value={perm.id} key={perm.id}>{perm.title}</Option>)
                        }) : <></>}
                    </Select> : <></>
                }
            </form>
        </Modal>
    )
}
