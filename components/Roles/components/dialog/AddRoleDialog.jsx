import {Button, Checkbox, Input, Modal, Select, Tag} from "antd";
import {useFormik} from "formik";
import {Fragment, useEffect} from "react";
import {useDispatch} from "react-redux";
import roleAction from "../../../../store/Role/role.action";
import {cloneDeep} from 'lodash';

export default function AddRoleDialog(props) {
    const dispatch = useDispatch();
    const roleForm = useFormik({
        initialValues: {
            title: '',
            permissions: [],
        },
        onSubmit: values => {
            if (props.role) {
                dispatch(roleAction.updateRoleRequested(props.role.id, values));
            } else {
                dispatch(roleAction.createRoleRequested(values));
            }
        }
    });

    const onCheckBoxChanges = (value) => {
        const permissions = cloneDeep(roleForm.values.permissions);
        if (value.target.checked) {
            permissions.push(value.target.value);
            roleForm.setFieldValue('permissions', permissions);
        } else {
            const index = permissions.findIndex(_perm => _perm === value.target.value);
            if (index > -1) {
                permissions.splice(index, 1);
                roleForm.setFieldValue('permissions', permissions);
            }
        }
    }

    useEffect(() => {
        roleForm.setValues({
            title: props.role ? props.role.title : '',
            permissions: props.role ? props.role.permissions : []
        });
    }, [props.role]);

    return (
        <Modal
            visible={props.isModalOpen}
            title={props.role ? `Update Role - ${props.role.title}` : 'Add Role'}
            onOk={() => props.showModal(false)}
            onCancel={() => props.showModal(false)}
            footer={[
                <Button key="back" onClick={() => props.showModal(false)}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={props.actionLoading}
                        disabled={!roleForm.isValid}
                        onClick={roleForm.handleSubmit}>
                    Save
                </Button>,
            ]}
        >
            <Input placeholder="Title" size="large" className="rounded-md" name="title" id="title"
                   onChange={roleForm.handleChange} value={roleForm.values.title}/>
            <div className="mt-8">
                {props.permissions.map(perm => {
                    if (perm.level !== 1) return;
                    return <Fragment key={perm.id}>
                        <div className="border-b"/>
                        <div className="my-8">
                            <Checkbox checked={roleForm.values.permissions.find(_perm => _perm === perm.id)}
                                      value={perm.id} onChange={value => onCheckBoxChanges(value)}><span
                                className="font-bold">{perm.name}</span></Checkbox>
                            <div className="flex justify-between">
                                {props.permissions.map(child => {
                                    if (child.parentId !== perm.id) return;
                                    return (<Checkbox value={child.id} key={child.id}
                                                      onChange={value => onCheckBoxChanges(value)}
                                                      checked={roleForm.values.permissions.find(_perm => _perm === child.id)}>{child.title}</Checkbox>)
                                })}
                            </div>
                        </div>
                    </Fragment>
                })}
            </div>
        </Modal>
    )
}
