import {Tabs} from "antd";
import Permission from "./presentational/Permission";
import Role from "./presentational/Role";

const {TabPane} = Tabs;


export default function Roles() {
    return (
        <div className="flex-1 px-4 py-2 bg-white rounded-xl h-auto">
            <Tabs defaultActiveKey="1">
                <TabPane tab="Roles" key="1">
                    <Role/>
                </TabPane>
                <TabPane tab="Permissions" key="2">
                    <Permission/>
                </TabPane>
            </Tabs>

        </div>
    )
}
