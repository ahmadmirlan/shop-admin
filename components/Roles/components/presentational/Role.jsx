import {useDispatch, useSelector} from "react-redux";
import roleAction from "../../../../store/Role/role.action";
import {Fragment, useEffect, useState} from 'react';
import {Button} from "antd";
import RoleTable from "./RoleTable";
import AddRoleDialog from "../dialog/AddRoleDialog";
import permissionAction from "../../../../store/Permission/permission.action";

export default function Role() {
    const dispatch = useDispatch();
    const [role, setRole] = useState(null);
    const {
        actionLoading,
        pageLoading,
        roles,
        dataLoaded,
        lastCreatedRole,
        lastQuery,
        lastQueryResult,
        isModalOpen
    } = useSelector(state => state.Role);

    const {allPermissions} = useSelector(state => state.Permission);

    let query = {
        "filter": {},
        "pageNumber": 0,
        "pageSize": 5,
        "sortField": "",
        "sortOrder": "desc"
    }

    const paginationChange = (page, pageSize) => {
        query.pageNumber = (page - 1);
        query.pageSize = pageSize;
    }

    const sizeChange = (current, size) => {
        query.pageNumber = 0;
        query.pageSize = size;
    }

    const onUpdateRole = (role) => {
        setRole(role);
        dispatch(roleAction.openRoleModal());
    };

    const onDeleteRole = (role) => {

    }

    const openDialog = (isOpen) => {
        if (isOpen) {
            dispatch(roleAction.openRoleModal());
        } else {
            dispatch(roleAction.hideRoleModal());
        }
    }

    useEffect(() => {
        if (!dataLoaded) {
            dispatch(roleAction.findRoleRequested(query));
        }
        if (!allPermissions.isLoaded) {
            dispatch(permissionAction.getAllPermissionRequested());
        }
    }, [dataLoaded, allPermissions]);

    return (
        <Fragment>
            <HeaderTitle showModal={openDialog}/>
            <RoleTable dataSource={roles} pageLoading={pageLoading} queryResult={lastQueryResult}
                       sizeChange={sizeChange} paginationChange={paginationChange} onDelete={onDeleteRole}
                       onUpdate={onUpdateRole}/>
            <AddRoleDialog isModalOpen={isModalOpen} role={role} actionLoading={actionLoading} showModal={openDialog}
                           permissions={allPermissions.data}/>
        </Fragment>
    )
}

const HeaderTitle = (props) => {
    return (
        <div className="flex py-4 px-2 items-center justify-between">
            <Button type="primary" onClick={() => props.showModal(true)}>Add Data</Button>
        </div>
    );
};
