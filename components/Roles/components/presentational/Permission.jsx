import {Button, Input, Modal} from "antd";
import {Fragment} from 'react';
import PermissionTable from "./PermissionTable";
import {useDispatch, useSelector} from "react-redux";
import permissionAction from "../../../../store/Permission/permission.action";
import {useEffect} from 'react';
import AddPermissionDialog from "../dialog/AddPermissionDIalog";
import {useState} from 'react';

const {Search} = Input;

export default function Permission() {
    const dispatch = useDispatch();
    const [permission, setPermission] = useState(null);
    const {
        actionLoading,
        pageLoading,
        permissions,
        dataLoaded,
        lastQueryResult,
        isModalOpen,
        parentPermission
    } = useSelector(state => state.Permission);
    let query = {
        "filter": {},
        "pageNumber": 0,
        "pageSize": 5,
        "sortField": "",
        "sortOrder": "desc"
    }

    const paginationChange = (page, pageSize) => {
        query.pageNumber = (page - 1);
        query.pageSize = pageSize;
        dispatch(permissionAction.findPermissionRequested(query));
    }

    const sizeChange = (current, size) => {
        query.pageNumber = 0;
        query.pageSize = size;
        dispatch(permissionAction.findPermissionRequested(query));
    }

    const onSearchByName = (text) => {
        query.filter = {name: text}
        query.pageNumber = 0;
        dispatch(permissionAction.findPermissionRequested(query));
    }

    const openModalDialog = (isShow) => {
        if (isShow) {
            setPermission(null);
            dispatch(permissionAction.openModalPermission());
        } else {
            dispatch(permissionAction.hideModalPermission())
        }
    }

    const onUpdatePermission = (permission) => {
        setPermission(permission);
        dispatch(permissionAction.openModalPermission());
    }

    const onDeletePermission = (permission) => {
        Modal.confirm({
            title: 'Are You Sure ?',
            content: `You are going to remove "${permission.name}" from permission`,
            onOk: () => {
                dispatch(permissionAction.deletePermissionRequested(permission))
            },
            okType: 'danger',
            okText: 'Remove'
        })
    }

    useEffect(() => {
        if (!dataLoaded) {
            dispatch(permissionAction.findPermissionRequested(query));
        }
        if (!parentPermission.isLoaded) {
            dispatch(permissionAction.findAllParentPermissionRequested());
        }
    }, [dataLoaded, parentPermission]);

    return (
        <Fragment>
            <HeaderTitle title="Add Data" openModalDialog={openModalDialog} onSearchByName={onSearchByName}/>
            <PermissionTable dataSource={permissions} pageLoading={pageLoading}
                             paginationChange={paginationChange} sizeChange={sizeChange}
                             queryResult={lastQueryResult} onDelete={onDeletePermission} onUpdate={onUpdatePermission}/>
            <AddPermissionDialog isModalOpen={isModalOpen} actionLoading={actionLoading} showModal={openModalDialog}
                                 parent={parentPermission.data} permission={permission}/>
        </Fragment>
    );
}

const HeaderTitle = (props) => {
    return (
        <div className="flex py-4 px-2 items-center justify-between">
            <Button type="primary" onClick={() => props.openModalDialog(true)}>{props.title}</Button>
            <div>
                <Search placeholder="Search..." allowClear onSearch={value => props.onSearchByName(value)} enterButton/>
            </div>
        </div>
    );
};
