import CustomTable from "../../../shared/components/CustomTable";
import {Button, Space, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";

export default function RoleTable(props) {
    const roleTableHeader = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: text => <p>{text}</p>,
        },
        {
            title: 'Core Role',
            dataIndex: 'isCoreRole',
            key: 'id',
            render: core => (
                core ? <Tag color='red'>CORE</Tag> : <></>
            )
        },
        {
            title: 'Action',
            key: 'id',
            render: (text, record) => (
                !record.isCoreRole ? <Space size="middle">
                    <Tooltip title="Edit">
                        <Button size="small" type="dashed" shape="circle" icon={<EditOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onUpdate(record)}/>
                    </Tooltip>
                    <Tooltip title="Delete">
                        <Button size="small" type="dashed" danger shape="circle" icon={<DeleteOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onDelete(record)}/>
                    </Tooltip>
                </Space> : <></>
            ),
        },
    ];
    return (
        <CustomTable dataSource={props.dataSource} columns={roleTableHeader} pageLoading={props.pageLoading}
                     queryResult={props.queryResult} sizeChange={props.sizeChange}
                     paginationChange={props.paginationChange}/>
    )
}
