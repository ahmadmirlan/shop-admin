import CustomTable from "../../../shared/components/CustomTable";
import {Button, Space, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";

export default function PermissionTable(props) {
    const permissionTableHeader = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
            render: text => <p>{text}</p>,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <p>{text}</p>
        },
        {
            title: 'Level',
            dataIndex: 'level',
            key: 'id',
            sorter: (a, b) => a.level - b.level,
            render: text => (
                <Tag color={text === 1 ? 'red' : 'green'}>{text}</Tag>
            )
        },
        {
            title: 'Parent Permission',
            dataIndex: 'parentId',
            key: 'id',
            render: parent => <span>{parent?.name}</span>,
        },
        {
            title: 'Core Permission',
            dataIndex: 'isCorePermission',
            key: 'id',
            render: core => (
                core ? <Tag color='red'>CORE</Tag> : <></>
            )
        },
        {
            title: 'Action',
            key: 'id',
            render: (text, record) => (
                !record.isCorePermission ? <Space size="middle">
                    <Tooltip title="Edit">
                        <Button size="small" type="dashed" shape="circle" icon={<EditOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onUpdate(record)}/>
                    </Tooltip>
                    <Tooltip title="Delete">
                        <Button size="small" type="dashed" danger shape="circle" icon={<DeleteOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onDelete(record)}/>
                    </Tooltip>
                </Space> : <></>
            ),
        },
    ]
    return (
        <CustomTable dataSource={props.dataSource} columns={permissionTableHeader} pageLoading={props.pageLoading}
                     paginationChange={props.paginationChange} sizeChange={props.sizeChange}
                     queryResult={props.queryResult}/>
    )
}
