import * as Yup from 'yup';

export const permissionSchema = Yup.object().shape({
    title: Yup.string()
        .required('title cannot be empty'),
    name: Yup.string()
        .required('name cannot be empty'),
    level: Yup.number()
        .required('please select level'),
    parentId: Yup.string()
})
