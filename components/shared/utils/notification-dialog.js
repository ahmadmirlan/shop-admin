import {Modal} from "antd";

export function confirm(props) {
    Modal.confirm({
        title: props.title,
        content: props.content,
        okText: props.okText ? props.okText : 'Ok',
        cancelText: props.cancelText ? props.cancelText : 'Cancel',
        okType: props.okType ? props.okType : 'primary'
    })
}
