import React from "react";
import {Table} from "antd";

export default function (props) {

    const {dataSource, columns, pageLoading, queryResult, paginationChange, sizeChange} = props;

    return (
        <Table dataSource={dataSource} columns={columns} rowKey="id" loading={pageLoading}
               pagination={{
                   defaultPageSize: 5,
                   showSizeChanger: true,
                   total: queryResult.totalElements ? queryResult.totalElements : 0,
                   pageSize: queryResult.pageSize,
                   onChange: (page, pageSize) => paginationChange(page, pageSize),
                   onShowSizeChange: (current, size) => sizeChange(current, size),
                   pageSizeOptions: ['5', '7', '10'],
               }}/>
    )
}
