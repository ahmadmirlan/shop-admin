import {useDispatch} from "react-redux";
import authAction from "../../../../store/Auth/auth.action";
import {useRouter} from "next/router";

export default function Header() {
    const dispatch = useDispatch();
    const router = useRouter();
    const onLogout = () => {
        dispatch(authAction.authLogout());
        router.push('/');
    }
    return (
        <div className="flex justify-end py-2 px-4">
            <div className="border border-black rounded-full p-2" onClick={() => onLogout()}>
                <img src="/assets/images/logo-black.png" className="w-12 h-12 rounded-full"/>
            </div>
        </div>
    );
}
