import {TagsFilled} from '@ant-design/icons';
import styled from 'styled-components';
import {useRouter} from 'next/router';
import {menuItems} from "../../../../helper/navigation.helper";
import {Fragment} from "react";

const ItemsStyled = styled.div`
  padding: 0 2rem;
  margin-top: 4rem;
`

export default function Sidebar(props) {
    const router = useRouter();
    const navigatePage = (path) => {
        router.push(path);
    };
    return (
        <div>
            <SidebarLogo/>
            <SidebarItems navigatePage={navigatePage} user={props.user}/>
        </div>
    )
}

const SidebarLogo = () => {
    return (
        <div className="p-4 animate-pulse flex justify-center">
            <img src="/assets/images/logo-black.png" className="w-24 h-24" alt="Logo"/>
        </div>
    );
}

const SidebarItems = (props) => {
    return (
        <ItemsStyled>
            {menuItems.map(menu => {
                return (<div className="flex flex-col items-center cursor-pointer mt-12"
                             onClick={() => props.navigatePage(menu.route)} key={menu.id}>
                        {renderSidebar(menu, props.user)}
                    </div>
                )
            })}
        </ItemsStyled>
    );
}

const renderSidebar = (menu, user) => {
    const roles = menu.roles;
    const role = roles.find(role => role.includes(user?.role?.title));
    if (menu.roles && menu.roles < 1 || role) {
        return (
            <Fragment>
                <img src={menu.icon.url} className="w-10 h-10" alt={menu.pageTitle}/>
                <span className="font-sans font-semibold">{menu.pageTitle}</span>
            </Fragment>
        )
    } else {
        return (<></>)
    }
}
