import Sidebar from "./Sidebar";
import Content from "./Content";
import {useSelector} from "react-redux";
import {useRouter} from "next/router";
import {useEffect} from 'react';

export default function Layout(props) {
    const router = useRouter()
    const {isLogged, user} = useSelector(state => state.Auth);
    useEffect(() => {
        /*if (!isLogged) {
            router.push('/auth/login');
        }*/
    }, [isLogged]);
    return (
        <div className="w-full h-screen p-8 bg-purple-50">
            <div className="h-full bg-white rounded-3xl p-2 flex">
                <Sidebar user={user}/>
                <Content>{props.children}</Content>
            </div>
        </div>
    )
}
