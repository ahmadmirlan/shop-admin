import Header from "./Header";

export default function Content(props) {
    return (
        <div className="flex-1 bg-gray-200 rounded-r-3xl">
            <Header/>
            <div className="py-2 px-4">
                {props.children}
            </div>
        </div>
    )
}
