import {Input, Button, Checkbox} from 'antd';
import styled from 'styled-components';

const RegisterStyle = styled.div`
  height: 500px;
  width: 600px;
  background-color: rgba();
  border-radius: 1rem;

  .register-container {
    display: flex;
    width: 100%;
    height: 100%;
    padding: 0;

    &__section1 {
      display: flex;
      flex-direction: column;
      flex-basis: 100%;
      width: 100%;
      height: 100%;
    }

    &__section2 {
      flex-basis: 50%;
      width: 100%;
      height: 100%;
      background-image: url("/assets/images/undraw_surfer_m6jb.png");
      background-size: cover;
    }
  }

  .register-title {
    display: flex;
    flex-basis: 12%;
    justify-content: center;
    align-items: center;

    &__logo {
      height: 4rem;
      width: 4rem;
    }

    &__text {
      margin-left: 1rem;
      font-size: 3rem;
      text-transform: uppercase;
      font-weight: 700;
      display: inline-block;
      background-image: linear-gradient(to right, black, black);
      -webkit-background-clip: text;
      color: transparent;
      letter-spacing: .7rem;
      transition: all .2s;
    }
  }

  .register-input {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-basis: 58%;
    padding: 0 2rem;

    &__group {
      display: flex;
      justify-content: space-around;
      width: 100%;
      
      input {
        &:not(:last-child) {
          margin-right: 1.5rem;
        }
      }

      &:not(:last-child) {
        margin-bottom: 20px;
      }
    }

    &__remember {
      align-self: flex-start;
    }
  }

  .register-action {
    display: flex;
    flex-direction: column;
    flex-basis: 30%;
    padding: 0 2rem;
    justify-content: flex-start;
    align-items: center;

    button {
      width: 40%;
    }

    span {
    }
  }
`

export default function Register() {
    return (
        <RegisterStyle>
            <div className="register-container shadow rounded-md">
                <div className="register-container__section1">
                    <div className="register-title">
                        <img src="/assets/images/logo-black.png" className="register-title__logo"/>
                        <span className="register-title__text">REGISTER</span>
                    </div>
                    <div className="register-input">
                        <div className="register-input__group">
                            <Input placeholder="First Name" size="large" className="rounded-lg"/>
                            <Input placeholder="Last Name" size="large" className="rounded-lg"/>
                        </div>
                        <div className="register-input__group">
                            <Input placeholder="Username" size="large" className="rounded-lg"/>
                            <Input placeholder="Email" size="large" className="rounded-lg"/>
                        </div>
                        <div className="register-input__group">
                            <Input placeholder="Password" type="password" size="large" className="rounded-lg"/>
                            <Input placeholder="Password" type="password" size="large" className="rounded-lg"/>
                        </div>
                        <Checkbox color="primary" className="register-input__remember">I Accept</Checkbox>
                    </div>
                    <div className="register-action">
                        <Button className="rounded-lg" type="primary">Login</Button>
                        <span className="m-2">Or</span>
                        <Button className="rounded-lg">Sign Up</Button>
                    </div>

                </div>
            </div>
        </RegisterStyle>
    );
}
