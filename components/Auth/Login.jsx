import {Input, Button, Checkbox} from 'antd';
import styled from 'styled-components';
import {useFormik} from "formik";
import {loginSchema} from "./schema/lgoin-schema";
import {useDispatch, useSelector} from "react-redux";
import authAction from "../../store/Auth/auth.action";
import {useEffect} from 'react';
import {useRouter} from "next/router";

const LoginStyle = styled.div`
  height: 400px;
  width: 400px;
  border-radius: 1rem;

  .login-title-text {
    margin-left: 1rem;
    font-size: 3rem;
    text-transform: uppercase;
    font-weight: 700;
    display: inline-block;
    background-image: linear-gradient(to right, black, grey);
    -webkit-background-clip: text;
    color: transparent;
    letter-spacing: .7rem;
    transition: all .2s;
  }

  .login-input {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    flex-basis: 58%;
    padding: 0 1rem;

    &__remember {
      align-self: flex-start;
      margin-left: 4rem;
    }
  }

  .login-button {
    width: 60%;
    color: #ffffff;
  }
`

export default function Login() {
    const dispatch = useDispatch();
    const router = useRouter();
    const {actionLoading, isLogged} = useSelector(state => state.Auth);
    const loginForm = useFormik({
        validationSchema: loginSchema,
        initialValues: {
            email: '',
            password: ''
        },
        onSubmit: values => {
            dispatch(authAction.loginRequested(values));
        }
    });

    useEffect(() => {
        if (isLogged) {
            router.push('/dashboard');
        }
    }, [isLogged]);

    return (
        <LoginStyle>
            <div className="flex shadow rounded-md w-full h-full">
                <div className="flex flex-col flex-1 w-full h-full">
                    <LoginTitle/>
                    <LoginInput loginForm={loginForm}/>
                    <LoginAction loginForm={loginForm} loading={actionLoading}/>
                </div>
            </div>
        </LoginStyle>
    )
}

const LoginTitle = () => {
    return (
        <div className="flex justify-center items-center pt-4">
            <img src="/assets/images/logo-black.png" className="w-16 h-16"/>
            <span className="login-title-text">LOGIN</span>
        </div>
    )
}

const LoginInput = (props) => {
    return (
        <div className="flex-1 flex flex-col justify-center px-14">
            <Input placeholder="Email" size="large" className="w-full rounded-lg" name="email" id="email"
                   value={props.loginForm.values.email} onChange={props.loginForm.handleChange}/>
            <Input placeholder="Password" type="password" size="large" className="w-full mt-8 rounded-lg"
                   name="password" id="password"
                   value={props.loginForm.values.password} onChange={props.loginForm.handleChange}/>
            <Checkbox className="mt-4">Remember me</Checkbox>
        </div>
    )
};

const LoginAction = (props) => {
    return (
        <div className="flex flex-col items-center items-start pb-8 px-8">
            <Button size="large" className="w-3/5 rounded-lg" disabled={!props.loginForm.isValid}
                    onClick={props.loginForm.handleSubmit} loading={props.loading}>
                Login
            </Button>
        </div>
    )
};
