import * as Yup from 'yup';

export const masterCategorySchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Min length 2')
        .required('Cannot be empty'),
});
