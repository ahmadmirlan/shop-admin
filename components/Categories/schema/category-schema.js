import * as Yup from 'yup';

export const categorySchema = Yup.object().shape({
    category: Yup.string()
        .min(2, 'Min length 2')
        .required('Cannot be empty'),
    masterCategory: Yup.string()
        .required('Please select master category'),
    status: Yup.string()
});
