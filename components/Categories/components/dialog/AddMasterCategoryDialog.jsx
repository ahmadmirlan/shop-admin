import {useEffect} from 'react';
import {Button, Input, Modal} from 'antd';
import {useFormik} from "formik";
import masterCategoryAction from "../../../../store/MasterCategory/master-category.action";
import {masterCategorySchema} from "../../schema/master-category-schema";
import {useDispatch} from "react-redux";

export default function AddMasterCategoryDialog(props) {
    const dispatch = useDispatch();
    const masterCategoryForm = useFormik({
        validationSchema: masterCategorySchema,
        initialValues: {
            name: props.masterCategory ? props.masterCategory.name : '',
        },
        onSubmit: (values) => {
            if (props.masterCategory) {
                dispatch(masterCategoryAction.updateMasterCategoryRequested({
                    ...props.masterCategory,
                    name: values.name
                }))
            } else {
                dispatch(masterCategoryAction.createMasterCategoryRequested({name: values.name}));
            }
        }
    });

    useEffect(() => {
        masterCategoryForm.setValues({
            name: props.masterCategory ? props.masterCategory.name : ''
        })
    }, [props.masterCategory]);

    return (
        <Modal
            visible={props.isModalOpen}
            title={props.masterCategory ? `Update Master Category - ${props.masterCategory.name}` : 'Add Master Category'}
            onOk={() => props.showModal(false)}
            onCancel={() => props.showModal(false)}
            footer={[
                <Button key="back" onClick={() => props.showModal(false)}>
                    Cancel
                </Button>,
                <Button key="submit" type="primary" loading={props.actionLoading} disabled={!masterCategoryForm.isValid}
                        onClick={masterCategoryForm.handleSubmit}>
                    Save
                </Button>,
            ]}
        >
            <div>
                <form onSubmit={masterCategoryForm.handleSubmit}>
                    <Input placeholder="Category name" size="large" className="rounded-md" name="name" id="name"
                           onChange={masterCategoryForm.handleChange} value={masterCategoryForm.values.name}/>
                </form>
            </div>
        </Modal>
    );
}
