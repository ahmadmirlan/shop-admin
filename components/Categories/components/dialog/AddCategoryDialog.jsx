import {useEffect} from 'react';
import {Button, Input, Modal, Select, Tag} from "antd";
import {useFormik} from "formik";
import {useDispatch} from "react-redux";
import categoryAction from "../../../../store/Category/category.action";
import {categorySchema} from "../../schema/category-schema";

const {Option} = Select;

export default function AddCategoryDialog(props) {
    const dispatch = useDispatch();
    const categoryForm = useFormik({
        validationSchema: categorySchema,
        initialValues: {
            category: props.category ? props.category.category : '',
            masterCategory: props.category ? props.category.masterCategory.id : '',
            status: props.category ? props.category.status : 'ACTIVE'
        },
        onSubmit: (values) => {
            if (props.category) {
                dispatch(categoryAction.updateCategoryRequested({...props.category, ...values}))
            } else {
                dispatch(categoryAction.createCategoryRequested(values));
            }
        }
    });

    useEffect(() => {
        categoryForm.setValues({
            category: props.category ? props.category.category : '',
            masterCategory: props.category ? props.category.masterCategory.id : '',
            status: props.category ? props.category.status : 'ACTIVE'
        });
    }, [props.category]);

    return (
        <Modal
            visible={props.isModalOpen}
            title={props.category ? `Update Category - ${props.category.category}` : 'Add Category'}
            onOk={() => props.showModal(false)}
            onCancel={() => props.showModal(false)}
            footer={[
                <Button key="back" onClick={() => props.showModal(false)}>
                    Return
                </Button>,
                <Button key="submit" type="primary" loading={props.actionLoading}
                        disabled={!categoryForm.isValid}
                        onClick={categoryForm.handleSubmit}>
                    Save
                </Button>,
            ]}
        >
            <form onSubmit={categoryForm.handleSubmit}>
                <Input placeholder="Category name" size="large" className="rounded-md" name="category" id="category"
                       onChange={categoryForm.handleChange} value={categoryForm.values.category}/>
                <Select
                    size="large"
                    showSearch
                    placeholder="Please select"
                    optionFilterProp="name"
                    name="masterCategory"
                    id="masterCategory"
                    onChange={value => categoryForm.setFieldValue('masterCategory', value)}
                    value={categoryForm.values.masterCategory}
                    filterOption={(input, option) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                    }
                    className="mt-4 w-full rounded-md"
                >
                    {props.masterCategories ? props.masterCategories.map(cat => {
                        return (<Option value={cat.id} key={cat.id}>{cat.name}</Option>)
                    }) : <></>}
                </Select>
                <Select
                    size="large"
                    placeholder="Select status"
                    name="status"
                    className="mt-4 w-1/2"
                    disabled={!props.category}
                    onChange={value => categoryForm.setFieldValue('status', value)}
                    id="status" value={categoryForm.values.status}>
                    <Option key="active" value="ACTIVE" id="active"><Tag color="green">ACTIVE</Tag></Option>
                    <Option key="disabled" value="DISABLED" id="disabled"><Tag color="red">DISABLED</Tag></Option>
                </Select>
            </form>
        </Modal>
    );
}
