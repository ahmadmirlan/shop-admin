import {Button, Space, Tag, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";
import CustomTable from "../../../shared/components/CustomTable";

export default function CategoryTable(props) {
    const categoryTableHeader = [
        {
            title: 'Name',
            dataIndex: 'category',
            key: 'id',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Master Category',
            dataIndex: 'masterCategory',
            key: 'id',
            render: (masterCategory) => (
                <>
                    <Tag color="green">
                        {masterCategory.name.toUpperCase()}
                    </Tag>
                </>
            ),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'id',
            render: status => (
                <RenderCategoryStatus status={status}/>
            ),
        },
        {
            title: 'Action',
            key: 'id',
            render: (text, record) => (
                <Space size="middle">
                    <Tooltip title="Edit">
                        <Button size="small" type="dashed" shape="circle" icon={<EditOutlined/>}
                                onClick={() => props.onUpdate(record)}
                                className="flex items-center justify-center"/>
                    </Tooltip>
                    <Tooltip title="Delete">
                        <Button size="small" type="dashed" danger shape="circle" icon={<DeleteOutlined/>}
                                className="flex items-center justify-center" onClick={() => props.onDelete(record)}/>
                    </Tooltip>
                </Space>
            ),
        },
    ];

    return (
        <CustomTable dataSource={props.dataSource} columns={categoryTableHeader} pageLoading={props.pageLoading}
                     queryResult={props.lastQueryResult}
                     paginationChange={props.paginationChange} sizeChange={props.sizeChange}/>
    );
}

const RenderCategoryStatus = (props) => {
    let color;

    switch (props.status.toUpperCase()) {
        case 'ACTIVE':
            color = 'green';
            break;
        case 'DISABLED':
            color = 'red';
            break;
        case 'DELETED':
            color = 'red';
            break;
        default:
            color = '';
            break;
    }

    return (
        <Tag color={color} key={props.status}>
            {props.status.toUpperCase()}
        </Tag>
    );
}
