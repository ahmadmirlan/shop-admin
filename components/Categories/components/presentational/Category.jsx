import {Button, Modal} from "antd";
import {Fragment, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import categoryAction from "../../../../store/Category/category.action";
import AddCategoryDialog from "../dialog/AddCategoryDialog";
import CategoryTable from "./CategoryTable";
import masterCategoryAction from "../../../../store/MasterCategory/master-category.action";

export default function Category() {
    const dispatch = useDispatch();
    const [category, setCategory] = useState(null);
    const {
        actionLoading,
        pageLoading,
        categories,
        lastQueryResult,
        isModalOpen,
        dataLoaded
    } = useSelector(state => state.Category);

    const {allMasterCategories} = useSelector(state => state.MasterCategory);

    let query = {
        "filter": {},
        "pageNumber": 0,
        "pageSize": 5,
        "sortField": "",
        "sortOrder": "desc"
    }

    const paginationChange = (page, pageSize) => {
        query.pageNumber = (page - 1);
        query.pageSize = pageSize;
        dispatch(categoryAction.loadAllCategoryRequested(query));
    }

    const sizeChange = (current, size) => {
        query.pageNumber = 0;
        query.pageSize = size;
        dispatch(categoryAction.loadAllCategoryRequested(query));
    }

    const onSearch = (text) => {
        query.filter = {name: text};
        query.pageNumber = 0;
        dispatch(categoryAction.loadAllCategoryRequested(query));
    }

    const showModal = (isShow) => {
        if (isShow) {
            setCategory(null);
            dispatch(categoryAction.openModalCreateCategory());
        } else {
            dispatch(categoryAction.hideModalCreateCategory());
        }
    };

    const onUpdate = (category) => {
        setCategory(category);
        dispatch(categoryAction.openModalCreateCategory());
    }

    const onDelete = (category) => {
        console.log(category, 'DELETED');
        Modal.confirm({
            title: 'Are you sure ?',
            content: `You are going to remove "${category.category}" data!`,
            okText: 'Remove',
            okType: 'danger',
            onOk: (() => {
                dispatch(categoryAction.removeCategoryRequested(category));
            })
        })
    }

    useEffect(() => {
        document.title = 'Categories';
        if (!dataLoaded) {
            dispatch(categoryAction.loadAllCategoryRequested(query));
        }
        if (!allMasterCategories || !allMasterCategories.isLoaded) {
            dispatch(masterCategoryAction.findAllMasterCategoriesRequested());
        }
    }, [dataLoaded, allMasterCategories]);

    return (
        <Fragment>
            <HeaderTitle title="Categories" showModal={showModal}/>
            <div className="overflow-auto">
                <CategoryTable dataSource={categories} pageLoading={pageLoading} lastQueryResult={lastQueryResult}
                               paginationChange={paginationChange} onUpdate={onUpdate} onDelete={onDelete}
                               sizeChange={sizeChange}/>
            </div>
            <AddCategoryDialog isModalOpen={isModalOpen} showModal={showModal} actionLoading={actionLoading}
                               masterCategories={allMasterCategories?.data} category={category}/>
        </Fragment>
    );
}

const HeaderTitle = (props) => {
    return (
        <div className="flex py-4 px-2 items-center">
            <Button type="primary" onClick={() => props.showModal(true)}>Add Data</Button>
        </div>
    );
};


