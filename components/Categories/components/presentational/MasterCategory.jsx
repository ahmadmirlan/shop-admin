import {Button, Modal, Input} from "antd";
import {Fragment, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import masterCategoryAction from "../../../../store/MasterCategory/master-category.action";
import AddMasterCategoryDialog from "../dialog/AddMasterCategoryDialog";
import MasterCategoryTable from "./MasterCategoryTable";

const {Search} = Input;

export default function MasterCategory() {
    const dispatch = useDispatch();
    const [masterCategoryData, setMasterCategory] = useState(null);
    let query = {
        "filter": {},
        "pageNumber": 0,
        "pageSize": 5,
        "sortField": "",
        "sortOrder": "desc"
    }

    const {
        actionLoading,
        pageLoading,
        masterCategories,
        lastQueryResult,
        isModalOpen
    } = useSelector(state => state.MasterCategory);

    const paginationChange = (page, pageSize) => {
        query.pageNumber = (page - 1);
        query.pageSize = pageSize;
        dispatch(masterCategoryAction.loadAllMasterCategoryRequested(query));
    }

    const sizeChange = (current, size) => {
        query.pageNumber = 0;
        query.pageSize = size;
        dispatch(masterCategoryAction.loadAllMasterCategoryRequested(query));
    }

    const onSearch = (text) => {
        query.filter = {name: text};
        query.pageNumber = 0;
        dispatch(masterCategoryAction.loadAllMasterCategoryRequested(query));
    }

    const updateMasterCategory = (masterCategory) => {
        setMasterCategory(masterCategory);
        dispatch(masterCategoryAction.openModalCreateCategory());
    }

    const deleteMasterCategory = (masterCategory) => {
        Modal.confirm({
            title: 'Are you sure ?',
            content: `You are going to remove ${masterCategory.name} data!`,
            okText: 'Remove',
            okType: 'danger',
            onOk: () => {
                dispatch(masterCategoryAction.removeCategoryRequested(masterCategory));
            }
        })
    }

    useEffect(() => {
        document.title = 'Categories';
        if (masterCategories.length < 1) {
            dispatch(masterCategoryAction.loadAllMasterCategoryRequested({}));
        }
    }, [masterCategories]);

    const showModal = (isShow) => {
        if (isShow) {
            setMasterCategory(null);
            dispatch(masterCategoryAction.openModalCreateCategory())
        } else {
            dispatch(masterCategoryAction.hideModalCreateCategory())
        }
    }

    return (
        <Fragment>
            <HeaderTitle title="Master Categories" showModal={showModal} search={onSearch}/>
            <div className="overflow-auto">
                <MasterCategoryTable dataSource={masterCategories} pageLoading={pageLoading}
                                     lastQueryResult={lastQueryResult} updateAction={updateMasterCategory}
                                     deleteAction={deleteMasterCategory}
                                     paginationChange={paginationChange} sizeChange={sizeChange}/>
            </div>
            <AddMasterCategoryDialog actionLoading={actionLoading} isModalOpen={isModalOpen} showModal={showModal}
                                     masterCategory={masterCategoryData}/>
        </Fragment>
    )
}

const HeaderTitle = (props) => {
    return (
        <div className="flex py-4 px-2 items-center justify-between items-center">
            <Button type="primary" onClick={() => props.showModal(true)}>Add Data</Button>
            <div>
                <Search
                    placeholder="Search..."
                    allowClear
                    onSearch={value => props.search(value)}
                    className="w-80"
                />
            </div>
        </div>
    );
};
