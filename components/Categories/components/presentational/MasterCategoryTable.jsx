import CustomTable from "../../../shared/components/CustomTable";
import {Button, Space, Tooltip} from "antd";
import {DeleteOutlined, EditOutlined} from "@ant-design/icons";

export default function MasterCategoryTable(props) {
    const masterCategoryTableHeader = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Tooltip title="Edit">
                        <Button size="small" type="dashed" shape="circle" icon={<EditOutlined/>}
                                className="flex items-center justify-center"
                                onClick={() => props.updateAction(record)}/>
                    </Tooltip>
                    <Tooltip title="Delete">
                        <Button size="small" type="dashed" danger shape="circle" icon={<DeleteOutlined/>}
                                className="flex items-center justify-center"
                                onClick={() => props.deleteAction(record)}/>
                    </Tooltip>
                </Space>
            ),
        },
    ];

    return (
        <CustomTable dataSource={props.dataSource} columns={masterCategoryTableHeader} pageLoading={props.pageLoading}
                     queryResult={props.lastQueryResult}
                     paginationChange={props.paginationChange} sizeChange={props.sizeChange}/>
    )
}
