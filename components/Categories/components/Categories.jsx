import {Tabs} from 'antd';
import Category from "./presentational/Category";
import MasterCategory from "./presentational/MasterCategory";

const {TabPane} = Tabs;

export default function Categories() {
    return (
        <div className="flex-1 px-4 py-2 bg-white rounded-xl h-auto">
            <Tabs defaultActiveKey="1">
                <TabPane tab="Categories" key="1">
                    <Category/>
                </TabPane>
                <TabPane tab="Master Categories" key="2">
                    <MasterCategory/>
                </TabPane>
            </Tabs>

        </div>
    )
}

