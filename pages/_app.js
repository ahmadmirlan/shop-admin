import '../styles/globals.css'
import 'antd/dist/antd.css';
import 'tailwindcss/dist/tailwind.css';
import {withRouter} from 'next/router';
import React from 'react';
import withRedux from 'next-redux-wrapper';
import {store, configureStore} from '../store/store';
import {Provider} from "react-redux";
import Boot from '../store/boot';
import Layout from "../components/shared/components/layout/Layout";

function MyApp({Component, pageProps, router}) {
    return (
        <Provider store={store}>
            {renderPage(Component, pageProps, router)}
        </Provider>
    )
}

const renderPage = (Component, pageProps, router) => {
    switch (router.pathname) {
        case '/auth/login':
            return (<PublicPage Component={Component} pageProps={pageProps}/>)
        case '/':
            return (<PublicPage Component={Component} pageProps={pageProps}/>)
        default:
            return (<ProtectedRoute Component={Component} pageProps={pageProps}/>)
    }
}

function ProtectedRoute({Component, pageProps}) {
    return (
        <Layout>
            <Component {...pageProps} />
        </Layout>
    )
}

function PublicPage({Component, pageProps}) {
    return (<Component/>)
}

// Init or call data before app boot up
Boot().then(() => MyApp);

export default withRouter(withRedux(configureStore)(MyApp));
