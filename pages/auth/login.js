import Login from "../../components/Auth/Login";
import styled from 'styled-components';

const LoginContainerStyled = styled.div`
  width: 99vw;
  height: 99vh;
  padding-top: 10rem;
  
  .login-container {
    display: flex;
    justify-content: center;
    width: 100%;
  }
`;

export default function () {
    return (
        <LoginContainerStyled>
            <div className="login-container">
                <Login/>
            </div>
        </LoginContainerStyled>
    )
}
