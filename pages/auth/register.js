import Register from "../../components/Auth/Register";
import styled from "styled-components";

const RegisterContainerStyled = styled.div`
  width: 99vw;
  height: 99vh;
  padding-top: 10rem;

  .register-container {
    display: flex;
    justify-content: center;
    width: 100%;
  }
`;

export default function () {
    return (
        <RegisterContainerStyled>
            <div className="register-container">
                <Register/>
            </div>
        </RegisterContainerStyled>
    )
}
