import {Spin} from "antd";
import {useEffect} from 'react';
import {useRouter} from "next/router";
import {useSelector} from "react-redux";

export default function Home() {
    const router = useRouter();
    const {isLogged} = useSelector(state => state.Auth);

    useEffect(() => {
        if (!isLogged) {
            router.push('/auth/login');
        } else {
            router.push('/dashboard');
        }
    }, [isLogged]);
    return (
        <div className="flex justify-center items-center h-screen w-screen">
            <Spin size="large"/>
        </div>
    )
}
