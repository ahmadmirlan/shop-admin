import Axios from 'axios';
import {authGateway} from "./base";
import {defineHeaders} from "./auth.helper";

const axios = Axios.create({
    baseURL: authGateway
});

export const findRoles = (query) => {
    return axios.post('/roles', query, defineHeaders()).then(resp => resp.data);
}

export const updateRole = (id, role) => {
    return axios.put(`/role/${id}`, role, defineHeaders()).then(resp => resp.data);
}

export const createRole = (role) => {
    return axios.post('/role', role, defineHeaders()).then(resp => resp.data);
}

export const getAllRoles = () => {
    return axios.get('/roles/all', defineHeaders()).then(resp => resp.data);
}
