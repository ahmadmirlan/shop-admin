import Axios from 'axios';
import {categoryGateway} from './base';
import {defineHeaders} from './auth.helper';

const axios = Axios.create({
    baseURL: categoryGateway
})

export const createNewCategory = (category) => {
    return axios.post(`/category`, category, defineHeaders()).then(resp => resp.data);
}

export const updateCategory = (category) => {
    return axios.put(`/category/${category.id}`, category, defineHeaders()).then(resp => resp.data);
}

export const findAllCategory = (query) => {
    return axios.post(`/categories`, query, defineHeaders()).then(resp => resp.data);
}

export const findNewCategoryById = (categoryId) => {
    return axios.get(`/categories/${categoryId}`, defineHeaders()).then(resp => resp.data);
}

export const removeCategory = (categoryId) => {
    return axios.delete(`/category/${categoryId}`, defineHeaders()).then(resp => resp.data);
}
