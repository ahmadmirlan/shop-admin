import Axios from 'axios';
import {categoryGateway} from './base';
import {defineHeaders} from './auth.helper';

const axios = Axios.create({
    baseURL: categoryGateway
})

export const createNewMasterCategory = (masterCategory) => {
    return axios.post(`/master/category`, masterCategory, defineHeaders()).then(resp => resp.data);
}

export const updateMasterCategory = (masterCategory) => {
    return axios.put(`/master/category/${masterCategory.id}`, masterCategory, defineHeaders()).then(resp => resp.data);
}

export const findAllMasterCategory = (query) => {
    return axios.post(`/master/categories`, query, defineHeaders()).then(resp => resp.data);
}

export const findMasterCategoryById = (categoryId) => {
    return axios.get(`/categories/${categoryId}`, defineHeaders()).then(resp => resp.data);
}

export const removeMasterCategory = (categoryId) => {
    return axios.delete(`/master/category/${categoryId}`, defineHeaders()).then(resp => resp.data);
}

export const loadAllMasterCategories = () => {
    return axios.get('/master/categories/all', defineHeaders()).then(resp => resp.data);
}
