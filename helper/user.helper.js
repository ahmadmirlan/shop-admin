import Axios from 'axios';
import {authGateway} from "./base";
import {defineHeaders} from "./auth.helper";

const axios = Axios.create({
    baseURL: authGateway,
});

export const findAllUsers = (query) => {
    return axios.post('/users', query, defineHeaders()).then(resp => resp.data);
}

export const createNewUser = (user) => {
    return axios.post('/user/add', user, defineHeaders()).then(resp => resp.data);
}

export const checkEmail = (email) => {
    return axios.get(`/auth/check/email/${email}`).then(resp => resp.data);
}

export const checkUsername = (username) => {
    return axios.get(`/auth/check/username/${username}`).then(resp => resp.data);
}

export const updateUser = (user, id) => {
    return axios.put(`/user/${id}`, user, defineHeaders()).then(resp => resp.data);
}
