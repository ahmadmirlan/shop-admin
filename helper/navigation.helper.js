export const menuItems = [
    {
        id: 1,
        pageTitle: 'Dashboard',
        route: '/dashboard',
        icon: {
            type: 'svg',
            url: '/assets/icons/dashboard.svg'
        },
        roles: [],
    },
    {
        id: 2,
        pageTitle: 'Categories',
        route: '/categories',
        icon: {
            type: 'svg',
            url: '/assets/icons/tag.svg'
        },
        roles: ['Admin', 'Product'],
    },
    {
        id: 3,
        pageTitle: 'Products',
        route: '/categories',
        icon: {
            type: 'svg',
            url: '/assets/icons/shirt.svg'
        },
        roles: ['Admin', 'Product'],
    },
    {
        id: 4,
        pageTitle: 'Users',
        route: '/users',
        icon: {
            type: 'svg',
            url: '/assets/icons/user.svg'
        },
        roles: ['Admin'],
    },
    {
        id: 5,
        pageTitle: 'Permissions',
        route: '/permissions',
        icon: {
            type: 'svg',
            url: '/assets/icons/shield.svg'
        },
        roles: ['Admin']
    },
    {
        id: 6,
        pageTitle: 'Setting',
        route: '/user/myprofile',
        icon: {
            type: 'svg',
            url: '/assets/icons/settings.svg'
        },
        roles: [],
    }
];
