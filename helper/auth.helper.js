import Axios from 'axios';
import {authGateway} from "./base";

const axios = Axios.create({
    baseURL: authGateway
})

export const login = (user) => {
    return axios.post(`/auth/login`, user).then(resp => resp.data);
}

export const validateToken = () => {
    return axios.get(`/user/myInfo`, defineHeaders()).then(resp => resp.data);
}

export const getCurrentToken = () => {
    const token = localStorage.getItem('auth_token_user');

    if (!token) {
        return '';
    }
    return token;
}

// define headers
export const defineHeaders = () => {
    const token = getCurrentToken();
    return {
        headers: {'Authorization': `Bearer ${token}`}
    };
};
