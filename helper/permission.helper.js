import Axios from 'axios';
import {authGateway} from "./base";
import {defineHeaders} from "./auth.helper";

const axios = Axios.create({
    baseURL: authGateway
});

export const findPermissions = (query) => {
    return axios.post('/permissions', query, defineHeaders()).then(resp => resp.data);
};

export const findAllParentPermission = () => {
    return axios.get('/permissions/parent', defineHeaders()).then(resp => resp.data);
};

export const createNewPermission = (permission) => {
    return axios.post('/permission', permission, defineHeaders()).then(resp => resp.data);
}

export const deletePermission = (permissionId) => {
    return axios.delete(`/permission/${permissionId}`, defineHeaders()).then(resp => resp.data);
}

export const updatePermission = (id, permission) => {
    return axios.put(`/permission/${id}`, permission, defineHeaders()).then(resp => resp.data);
}

export const getAllPermissions = () => {
    return axios.get('/permissions/all', defineHeaders()).then(resp => resp.data);
}
